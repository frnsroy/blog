"""
blog.gen_files.py
Francois Roy
January 25, 2019
"""
import sys
import click
from blog import __version__
from blog.files_metal_semi import *


def print_version(ctx, param, value):
    r"""Prints the version and exits the program in the callback.

    :param param:
    :type param:
    :param ctx: Click internal object that holds state relevant for the script execution.
    :type ctx: click.context
    :param value: Close the program without printing the version if False.
    :type value: bool
    """
    if not value or ctx.resilient_parsing:
        return
    click.echo('blog {} (Python {})'.format(
        __version__,
        sys.version[:3]
    ))
    ctx.exit()


@click.command()
@click.option(
    '-p', '--post',
    help="The post submodule."
)
@click.option(
    '-v', '--version',
    is_flag=True, help='Show version information and exit.',
    callback=print_version, expose_value=False, is_eager=True,
)
def main(post):
    r"""Generate the files for the blog."""
    if post == 'metal_semi':
        anim_contact(101, 101)
        anim_n()
        anim_p()
        anim_transmission_coefficient()
        wave_function()
        current_density()


if __name__ == '__main__':
    main()
