# -*- coding: utf-8 -*-
"""
blog.plot_directive.sin.py
September 13, 2018
@author Francois Roy
"""
import matplotlib.pyplot as plt
import numpy as np
from metal_semi.contact import barrier


# Data for plotting
t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2 * np.pi * t)

fig, ax = plt.subplots()
ax.plot(t, s)

ax.set(xlabel='time (s)', ylabel='voltage (mV)',
       title='Waveform')
ax.grid()

# fig.savefig(r"../sinus.png")
plt.show()
