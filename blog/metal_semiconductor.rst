***************************
Metal-Semiconductor Contact
***************************

.. contents::
    :depth: 2

.. post:: November 6, 2016
   :tags: Semiconductors, Tunneling
   :category: Physics
   :author: François Roy
   :location: Roxbury
   :language: en
   :excerpt: 1
   :image: 1
   :exclude:

    This post present a general method to define metal-semiconductorcontacts
    for different metals and semiconductors of various carrier concentrations.

When a metal and a semiconductor are put into contact, a potential barrier of
length :math:`x_b` is form limiting charge transfer between the two materials.
:ref:`Animation 1<animation1>` shows the formation of the barrier as the
metal and bulk semiconductor are brought into contact. Note that we
consider a simple one-dimensional model where :math:`x` is the direction
pointing outward of the metal surface.

.. raw:: html

    <video width="600" height="450" align="center" controls loop>
    <source src="_static/contact.mp4" type="video/mp4">
    </video>

.. _animation1:

`Animation 1: Bending of the energy levels in the semiconductor as the two
material are put into contact. The blue and green line respectively
represent the bottom of the conduction band and the top of the valence
band. The magenta arrow shows the direction of the electron current as
the materials get into contact.`

Under certain circumstances, charges can cross the barrier through quantum
tunneling effect. This blog will show how to approximate the shape of the
potential barrier in order to obtain an analytic expression for the
wavefunction and the transmission coefficient for both holes and electrons.
Using these transmission coefficients and the Fermi-Dirac distribution, it is
possible to define the hole and current density at the metal-semiconductor
interface as a function of the bias voltage.

The list of symbols used for this blog is available in the
:ref:`Symbols<metal_semi_symbols>` section.


Barrier Definition
==================

It is possible to estimate the shape of the potential barrier using the
depletion model for an abrupt junction. In this model, we assume that the
charge density :math:`\rho` is uniform in the barrier region, i.e. for
:math:`x\in[0,x_{b}]`. Note also that the electron barrier height is constant
and defined by:

.. math:: \phi_{b,e}=\Phi-\chi
    :label: eq_bd1

where :math:`\Phi` is the metal work function, and :math:`\chi` is the
semiconductor electron affinity. The hole barrier height is just the
difference between the band-gap :math:`E_g` and the electron barrier height,
i.e.

.. math:: \phi_{b,h}=E_g-\phi_{b,e}
    :label: eq_bd2

The charge density is defined by:

.. math:: \rho=q\left(p-n+N_d-N_a\right)=qN_d^+
    :label: eq_bd3

Where :math:`q` is the elementary charge, :math:`p`, :math:`n`, :math:`N_d`,
and :math:`N_a` are respectively the hole, electron, donor, and acceptor
number densities. Using :eq:`eq_bd3` in Poisson's equation
, :math:`-\nabla(\epsilon\nabla V)=\rho` we get:

.. math:: -\frac{\partial}{\partial x}\left(\epsilon\frac{\partial}
    {\partial x}V\right)=qN_d^+,\quad\epsilon=\epsilon_0\epsilon_r
    :label: eq_bd4

Where :math:`V` is the electric potential and
:math:`\epsilon=\epsilon_0\epsilon_r` is the electric permittivity of the
semiconductor. Integrating :eq:`eq_bd4` on both side from :math:`x` to
:math:`x_b` gives:

.. math:: -\int_x^{x_b}\frac{\partial}{\partial x'}\left(\frac{\partial}
    {\partial x'}V\right)\partial x'=\frac{qN_d^+}{\epsilon}\int_x^{x_b}
    \partial x'
    :label: eq_bd5

Using the definition of the electric field :math:`\mathbf{E}=-\nabla V` we
get:

.. math:: E_x(x)=\frac{qN_d^+}{\epsilon}(x_b-x)
    :label: eq_bd6

Integrating :eq:`eq_bd5` another time,

.. math:: -\int_x^{x_b}\frac{\partial}{\partial x'}V\partial x'=
    \frac{qN_d^+}{\epsilon}\int_x^{x_b}\left(x_b-x'\right)\partial x'
    :label: eq_bd7

gives a parabolic electric potential distribution:

.. math:: V=\frac{qN_d^+}{2\epsilon}\left(x_b-x\right)^2
    :label: eq_bd8

From :eq:`eq_bd8`, we can see that the potential is maximum at the interface
(at :math:`x=0`).

The built-in potential :math:`V_\textrm{bi}` is defined as the maximum
potential under no applied voltage, i.e. when
:math:`qV_a = \mathcal{E}_{f,\textrm{I}}-\mathcal{E}_{f\textrm{II}} = 0`,
where :math:`\mathcal{E}_{f,\textrm{I}}` and
:math:`\mathcal{E}_{f,\textrm{II}}` respectively represent the Fermi level in
the metal and in bulk semiconductor.

.. math:: V_\textrm{bi}=\frac{1}{q}\left(\Phi_{b,e}-
    \mathcal{E}_{c,\textrm{eq}}\right)=\frac{qN_d^+}{2\epsilon}x_b^2
    :label: eq_bd9

The barrier length varies when an external voltage is applied, i.e.
:math:`x_{b}=x_b(V_a)`. Taking the Fermi level in the bulk semiconductor as
the energy zero (:math:`\mathcal{E}_{f,\textrm{II}}=0`), we can estimate the
bottom of the conduction band in the barrier region by:

.. math:: \mathcal{E}_c(x,V_a)=q^2\frac{N_d^+}{2\epsilon}
    \left(x_{b}(V_a)-x\right)^2+\mathcal{E}_{c,\textrm{eq}},\quad
    V_\textrm{bi} \geq V_a 
    :label: eq_bd10

where

.. math:: x_{b}(V_a) = \left\{
    \begin{array}{rl}
    \sqrt{\frac{2\epsilon}{q|N_d^+|}\left(V_\textrm{bi}-V_a\right)}
    &\text{if } V_\textrm{bi} \geq V_a,\\
    \sqrt{\frac{2\epsilon}{q|N_d^+|}\left(V_a-V_\textrm{bi}\right)}
    &\text{otherwise}.
    \end{array} \right.
    :label: eq_bd11

Using the following change of variables:

.. math:: \eta&=\mathcal{E}/k_BT\\u_a
    &=qV_a/k_BT\\ \xi
    &=x/x_b
    :label: eq_bd12

Where :math:`k_B` is the Boltzmann constant, and :math:`T` is the lattice
temperature. Equation :eq:`eq_bd10` can be rewritten in a non dimensional
form as:

.. math:: \eta_c(\xi,u_a)=\frac{x_{b}(u_a)^2}{2\lambda_D^2}(1-\xi)^2+
    \eta_{c,\textrm{eq}},\quad u_\textrm{bi} \geq u_a
    :label: eq_bd13

Where :math:`\lambda_D=\sqrt{\epsilon k_BT/q^2N_d^+}` is the Debye length.

Equation :eq:`eq_bd13` defines the potential barrier for both the conduction
and valence bands. The latter is defined as:

.. math:: \eta_v(\xi,u_a)=\eta_c(\xi,u_a)-\eta_g,\quad u_\textrm{bi}
    \geq u_a
    :label: eq_bd14

More generally, we have


.. math:: \eta_c(\xi,u_a) = \left\{
    \begin{array}{rl}
    \frac{x_{b}(u_a)^2}{2\lambda_D^2}(1-\xi)^2+\eta_{c,\textrm{eq}}&
    \text{if } u_\textrm{bi} \geq u_a,\\
    \eta_v(\xi,u_a)+\eta_g & \text{otherwise}.
    \end{array} \right.
    :label: eq_bd15

and

.. math:: \eta_v(\xi,u_a) = \left\{
    \begin{array}{rl}
    \eta_c(\xi,u_a)-\eta_g& \text{if } u_\textrm{bi} \geq u_a,\\
    \eta_{v,\textrm{eq}}-\frac{x_{b}(u_a)^2}{2\lambda_D^2}(1-\xi)^2 &
    \text{otherwise}.
    \end{array} \right.
    :label: eq_bd16


:ref:`Animation 2<animation2>` shows the variation of the energy levels
of a type-n semiconductor in contact with a metal under an external voltage
:math:`V_a` varying from -1 V to 1 V. Note that the levels' bending change
direction when :math:`V_a = V_\textrm{bi}` or when
:math:`E_{fm}=-V_\textrm{bi}` (the dashed-magenta line located in region I).
This is also where the holes' barrier shows up.

.. raw:: html

    <video width="600" height="450" align="center" controls loop>
    <source src="_static/ntype.mp4" type="video/mp4">
    </video>

.. _animation2:

`Animation 2: Bending of the energy levels in a n-type semiconductor as a
function of the applied potential. The blue and green line respectively
represent the bottom of the conduction band and the top of the valence band.
The magenta line shows the negative of the non-dimensionalized built-in
potential. The dashed black line represents the Fermi level in the metal and
the solid black line the Fermi level in the bulk semiconductor.`

:ref:`Animation 3<animation3>` shows the same variation but for a p-type
semiconductor with :math:`N_a=1e18~cm^{-3}`.

.. raw:: html

    <video width="600" height="450" align="center" controls loop>
    <source src="_static/ptype.mp4" type="video/mp4">
    </video>

.. _animation3:

`Animation 3: Bending of the energy levels in a p-type semiconductor as a
function of the applied potential. The blue and green line respectively
represent the bottom of the conduction band and the top of the valence band.
The magenta line shows the negative of the non-dimensionalized built-in
potential. The dashed black line represents the Fermi level in the metal and
the solid black line the Fermi level in the bulk semiconductor.`

Note that the electron barrier exists only when :math:`V_\textrm{bi}\geq V_a`,
and the hole barrier only when :math:`V_\textrm{bi}< V_a`. When the
conduction band is depleted (:math:`V_\textrm{bi}\geq V_a`) electrons can
tunnel from the barrier region to region I. Similarly, when the valence band
is depleted (:math:`V_\textrm{bi}< V_a`) the holes can tunnel from the
barrier region to region I.

Wavefunctions
==============

The movement of a charged particle in a crystal lattice is determined by the
time-dependent Schrödinger equation (TDSE).

.. math:: i\hbar\frac{\partial}{\partial t}\psi(x,t) =
    \left[-\frac{\hbar^2}{2m}\frac{\partial^2}{\partial x^2}+U(x,t)\right]
    \psi(x,t)
    :label: eq_wf1

where :math:`i^2=-1` is the imaginary unit, :math:`\psi(x,t)` is the wave
function (electrons or holes), :math:`m` is the effective mass of the charged
particle and :math:`U(x,t)` is the particle's potential energy (a real value).

Outside of the barrier region
------------------------------

In order to cross the potential barrier, charged particles located in region
I/II must be free to leave. For a free particle submitted to no forces,
the potential energy is constant, i.e.

.. math:: U(x,t)=U_0

For a time-independent potential, the TDSE -- equation :eq:`eq_wf1`, is
separable. The latter can be used to define the time-independent Schrödinger
equation (TISE) that admits solutions in the form of plane travelling waves,
i.e.

.. math:: \psi(x)=a\exp\left(ikx\right)
    :label: eq_wf3

.. toggle-header::
    :header: see details

        The separable 1D-TDSE is expressed as:

        .. math:: i\hbar\frac{\partial}{\partial t}\psi(x,t) =
            \left[-\frac{\hbar^2}{2m}\frac{\partial^2}{\partial x^2}+
            U(x)\right]\psi(x,t)
            :label: eq_wfa1

        We can separate the position and time dependence of the TDSE with the
        product wavefunction defined as:

        .. math:: \psi(x,t)=\psi(x)f(t)
            :label: eq_wfa2

        using :eq:`eq_wfa2`, :eq:`eq_wfa1` becomes

        .. math:: \begin{split}-\frac{\hbar^2}{2m}f(t)
            \frac{\partial^2}{\partial x^2}\psi(x)+U(x)\psi(x)f(t)=&
            \\i\hbar\psi(x)\frac{\partial}{\partial t}f(t)\end{split}
            :label: eq_wfa3

        Dividing both side of :eq:`eq_wfa3` by :eq:`eq_wfa2` leads to

        .. math:: -\frac{\hbar^2}{2m}\frac{1}{\psi(x)}
            \frac{\partial^2}{\partial x^2}\psi(x)+U(x)=i\hbar\frac{1}
            {f(t)}\frac{\partial}{\partial t}f(t)
            :label: eq_wfa4

        Using the separation constant :math:`\Lambda` shows that

        .. math:: -\frac{\hbar^2}{2m}\frac{1}{\psi(x)}\frac{\partial^2}
            {\partial x^2}\psi(x)+U(x)&=\Lambda\\i\hbar\frac{1}{f(t)}
            \frac{\partial}{\partial t}f(t)&=\Lambda
            :label: eq_wfa5

        Integrating the time-dependent part gives

        .. math:: f(t)=\exp\left(-i\frac{\Lambda}{\hbar}t\right)
            :label: eq_wfa6

        which represents an harmonic oscillator of angular frequency
        :math:`\omega=\Lambda/\hbar`. From the De Broglie postulate, we have

        .. math:: \mathcal{E}=\hbar \omega
            :label: eq_wfa7

        and the separation constant :math:`\Lambda` is just the energy of the
        system, i.e. :math:`\Lambda = \mathcal{E}`.

        The time-independent part of :eq:`eq_wfa5` is the time-independent
        Schrödinger equation (TISE), i.e.

        .. math:: \left[-\frac{\hbar^2}{2m}\frac{\partial^2}{\partial x^2}+
            U(x)\right]\psi(x)=\mathcal{E}\psi(x)
            :label: eq_wfa8

        Rearranging :eq:`eq_wfa8`, we have

        .. math:: \frac{\partial^2}{\partial x^2}\psi(x)=
            -\frac{2m}{\hbar^2}\left(\mathcal{E}-U(x)\right)\psi(x)
            :label:`eq_wfa9`

        using the momentum definition, i.e.
        :math:`p(x)=\sqrt{2m\left(\mathcal{E}-U(x)\right)}`, we get

        .. math:: \frac{\partial^2}{\partial x^2}\psi(x)=
            -\frac{p^2(x)}{\hbar^2}\psi(x)
            :label: eq_wfa10

        The general solution of :eq:`eq_wfa10` is a superposition of plane
        wave packets, i.e.

        .. math:: \psi_n(x)=\sum_n a_n\exp\left(ik_nx\right)
            :label: eq_wfa11

        where :math:`k_n` is the :math:`n` wave vector defined by

        .. math:: k_n=\frac{p_n}{\hbar}=\frac{1}{\hbar}
            \sqrt{2m\left(\mathcal{E}_n-U(x)\right)}
            :label: eq_wfa12

where :math:`a` is the amplitude of the wave and
:math:`k_e=\frac{1}{\hbar}\sqrt{2m_e\left(\mathcal{E}-U_\textrm{min}\right)}`
is the wave vector for the electrons. For holes we have
:math:`k_h=\frac{1}{\hbar}\sqrt{2m_h\left(U_\textrm{max}-\mathcal{E}\right)}`.
Electrons are considered "free" if their energy is above
:math:`U_\textrm{min}`. We consider holes "free" if their energy is below
:math:`U_\textrm{max}`. Note that we only consider particles with momentum
directed in the :math:`\pm x` direction.

In the barrier region
---------------------

From the time-independent Schrödinger equation (TISE).  We can define the
wavefunctions in the one-dimensional parabolic barrier described above.

Note that we consider that no collision occur in the barrier
(transverse momentum and energy are conserved) and that the particle
entering/leaving the barrier only have their momentum directed in the
:math:`\pm x` direction.

For electrons we have

.. math:: -\frac{\hbar^2}{2m}\frac{\partial^2\psi(x)}{\partial x^2}+
    \mathcal{E}_c(x,V_a)\psi(x)=\mathcal{E}\psi(x)
    :label: eq_wf4

Where the electron potential energy corresponds to the bottom of the
conduction band (region II). For holes it is the top of the valence band.

It is convenient to scale :eq:`eq_wf4` using the change of variables:

.. math:: \eta&=\mathcal{E}/k_BT\\u_a&=qV_a/k_BT\\ \xi&=x/x_b
    :label: eq_wf5

With these variables, the equation :math:`\partial x = x_b\partial \xi` and
some  rearrangements, equation :eq:`eq_wf4` becomes:

.. math:: \frac{\partial^2\psi(\xi)}{\partial \xi^2}-\frac{2m k_BTx_b^2}
    {\hbar^2}\left(\eta_{c}(\xi,u_a)-\eta\right)\psi(\xi)=0
    :label: eq_wf6

For a parabolic barrier, the potential is described using :eq:`eq_bd10`.
Substituting the nondimensionalized version of :eq:`eq_bd10` in 
:eq:`eq_wf6` gives:

.. math:: \begin{split}\frac{\partial^2\psi(\xi)}{\partial \xi^2}-
    \frac{2m k_BTx_b(u_a)^2}{\hbar^2}\Bigg(\frac{x_b(u_a)^2}{2\lambda_D^2}
    \left(1-\xi\right)^2+&\\\eta_{c,\textrm{eq}}-\eta\Bigg)\psi(\xi)=0
    \end{split}
    :label: eq_wf7

Note that the barrier only exists if :math:`u_\textrm{bi}\geq u_a`.

Equation :eq:`eq_wf7` can be expressed in terms of the Parabolic Cylinder
Functions (PCFs) :cite:`a-DLMF`. Some manipulations must be performed
to get there.

First we simplify :eq:`eq_wf7` with the following change of variables:

.. math:: \alpha^2&=2m k_BTx_b^2(u_a)/\hbar^2\\4\lambda_s^4&=
    2\lambda_D^2\left(\alpha^2x_b^2(u_a)\right)^{-1}
    :label: eq_wf8

Which gives:

.. math:: \frac{\partial^2\psi(\xi)}{\partial \xi^2}-\left(\frac{1}
    {4\lambda_S^4}\left(1-\xi\right)^2-\alpha^2
    \left(\eta-\eta_{c,\textrm{eq}}\right)\right)\psi(\xi)=0
    :label: eq_wf9

Then using another change of variable:

.. math:: \xi_s&=\left(1-\xi\right)/\lambda_s\\ \kappa_s^2&=
    \left(\eta-\eta_{c,\textrm{eq}}\right)\left(\alpha^2\lambda_s^2\right)
    :label: eq_wf10

with the following properties:

.. math:: \frac{\partial \xi_s}{\partial \xi}&=-\frac{1}{\lambda_s}\\
    \frac{\partial^2 \xi_s}{\partial \xi^2}&=\frac{\partial}{\partial \xi}
    \left(-\frac{1}{\lambda_s}\right)=0\\ \frac{\partial^2 \psi(\xi)}
    {\partial \xi^2}&=\frac{\partial^2 \psi(\xi_s)}{\partial\xi_s^2}
    \left(\frac{\partial \xi_s}{\partial\xi}\right)^2+
    \frac{\partial \psi(\xi_s)}{\partial \xi_s}\frac{\partial^2\xi_s}
    {\partial \xi^2}
    :label: eq_wf11

we can rewrite equation :eq:`eq_wf9` as:

.. math:: \frac{\partial^2 \psi(\xi_s)}{\xi_s^2}-\left(\frac{\xi_s^2}{4}-
    \kappa_s^2\right)\psi(\xi_s)=0
    :label: eq_wf12

Which is one of the PCFs differential equation form.

Equation :eq:`eq_wf12` admits a solution of the form:

.. math:: \psi(\xi_s)=\alpha_s\mathcal{U}(-\kappa_s^2,\xi_s)+
    \beta_s\mathcal{V}(-\kappa_s^2,\xi_s),\quad{\xi_s\in[0,\frac{1}
        {\lambda_s}]}
    :label: eq_wf13

where :math:`\mathcal{U}(-\kappa_s^2,\xi_s)` and
:math:`\mathcal{V}(-\kappa_s^2,\xi_s)` are the PCFs and where
:math:`\alpha_s` and :math:`\beta_s` are constants determined by the
boundary conditions.

A similar result is obtained for holes.

Using the matching conditions
-- see :ref:`Matching Conditions<matching_conditions>`, we can plot the
electron probability distribution :math:`\psi\psi^\ast` in the barrier region.

.. _figure1:

.. figure:: _static/fig_psi.png
   :figwidth: 100%
   :alt: probability density distribution

   Figure 1: Probability density distribution for electrons in the barrier
   region of a n-type semiconductor.

.. _matching_conditions:

Matching Conditions
-------------------

The current density is given by:

.. math:: J_\xi(\xi)=q\frac{\hbar x_b}{i2m}\left(\psi(\xi)^\ast
    \frac{\partial \psi(\xi)}{\partial \xi}-\psi(\xi)
    \frac{\partial \psi(\xi)^\ast}{\partial \xi}\right)
    :label: eq_mc1

.. toggle-header::
    :header: see details

        If we multiply both side of equation :eq:`eq_wf1` by
        :math:`\psi^\ast` we obtain:

        .. math:: i\hbar\psi^\ast\frac{\partial}{\partial t}\psi =
            -\frac{\hbar^2}{2m}\psi^\ast\frac{\partial^2}{\partial x^2}\psi
            +U(x,t)\psi^\ast\psi
            :label: eq_mca1

        Where we deliberately omit the :math:`x` and :math:`t` dependence of
        the wave function to simplify the notation, i.e. :math:`\psi=\psi`.

        Similarly, if we multiply the complex conjugate of :eq:`eq_wf1` by
        :math:`\psi(x,t)` we get:

        .. math:: -i\hbar\psi\frac{\partial}{\partial t}\psi^\ast =
            -\frac{\hbar^2}{2m}\psi\frac{\partial^2}{\partial x^2}\psi^\ast+
            U(x,t)\psi\psi^\ast
            :label: eq_mca2

        Subtracting :eq:`eq_mca2` from :eq:`eq_mca1` we obtain:

        .. math:: \begin{split}i\hbar\left(\psi^\ast\frac{\partial}
            {\partial t}\psi+ \psi\frac{\partial}{\partial t}\psi^\ast\right)
            =&\\ -\frac{\hbar^2}{2m}\left(\psi^\ast\frac{\partial^2}
            {\partial x^2}+\psi\frac{\partial^2}{\partial x^2}\psi^\ast\right)
            \end{split}
            :label: eq_mca3

        With

        .. math:: \frac{\partial}{\partial t}\left(\psi^\ast\psi\right)&=\
            psi^\ast\frac{\partial}{\partial t}\psi+\psi\frac{\partial}
            {\partial t}\psi^\ast\\ \frac{\partial}{\partial x}
            \left(\psi^\ast\frac{\partial}{\partial x}-\psi\frac{\partial}
            {\partial x}\psi^\ast\right)&=\psi^\ast\frac{\partial^2}
            {\partial x^2}-\psi\frac{\partial^2}{\partial x^2}\psi^\ast
            :label: eq_mca4

        Equation :eq:`eq_mca3` can be rewritten as: 

        .. math:: \frac{\partial}{\partial t}\psi^\ast\psi+\frac{\hbar}{i2m}
            \frac{\partial}{\partial x}\left(\psi^\ast
            \frac{\partial}{\partial x}\psi-\psi\frac{\partial}{\partial x}
            \psi^\ast\right)=0
            :label: eq_mca5

        With :math:`\rho=\psi q\psi^\ast` and by direct analogy to the charge
        conservation
        :math:`\frac{\partial}{\partial t}\rho+\nabla\cdot \mathbf{J}=0`,
        we get:

        .. math:: J_x=q\frac{\hbar}{i2m}\left(\psi^\ast\frac{\partial}
            {\partial x}\psi-\psi\frac{\partial}{\partial x}\psi^\ast\right)
            :label: eq_mca6

        Using :math:`\xi=x/x_b` and :math:`\partial \xi = \partial x/x_b` we
        get

        .. math:: J_\xi=q\frac{\hbar x_b}{i2m}\left(\psi(\xi)^\ast
            \frac{\partial \psi(\xi)}{\partial \xi}-\psi(\xi)
            \frac{\partial \psi(\xi)^\ast}{\partial \xi}\right)
            :label: eq_mca7

From the current continuity at the interface we have:

.. math:: J_{\xi,\textrm{I}}(0^-)&=J_{\xi,b}(0^+)\\J_{\xi,b}(1^-)&=
    J_{\xi,\textrm{II}}(1^+)
    :label: eq_mc2

At :math:`\xi=0`, we have

.. math:: \frac{1}{m_\textrm{I}}
    \left(\psi_\textrm{I}(0)^\ast
    \left(\frac{\partial \psi_\textrm{I}(\xi)}{\partial \xi}\right)_{\xi=0} -
    \psi_\textrm{I}(0)\left(\frac{\partial \psi_\textrm{I}(\xi)^\ast}
    {\partial \xi}\right)_{\xi=0}\right)=& \\
    \frac{1}{m_b}\left(\psi_b(0)^\ast \left(
    \frac{\partial \psi_b(\xi)}{\partial \xi}\right)_{\xi=0}-
    \psi_b(0)\left(\frac{\partial \psi_b(\xi)^\ast}{\partial \xi}
    \right)_{\xi=0}\right)
    :label: eq_mc3

Where the indices :math:`_\textrm{I}` and :math:`_{b}` respectively refer to
the metal and barrier region. Adding :eq:`eq_mc3` to itself gives:

.. math:: \begin{split}\frac{1}{m_\textrm{I}}\left(\psi_\textrm{I}(0)^\ast
    \left(\frac{\partial \psi_\textrm{I}(\xi)}{\partial \xi}\right)_{\xi=0}
    \right)=&\\\frac{1}{m_b}\left(\psi_b(0)^\ast\left(\frac{\partial
    \psi_b(\xi)}{\partial \xi}\right)_{\xi=0}\right)\end{split}
    :label: eq_mc4

Which is only true if:

.. math:: \psi_\textrm{I}(0)&=\psi_b(0)\\
    \frac{1}{m_\textrm{I}}\left(\frac{\partial \psi_\textrm{I}(\xi)}
    {\partial \xi}\right)_{\xi=0}&=\frac{1}{m_b}
    \left(\frac{\partial \psi_b(\xi)}{\partial \xi}\right)_{\xi=0}
    :label: eq_mc5

A similar approach can be taken to get the continuity conditions at
:math:`\xi=1`, i.e. the barrier-semiconductor interface:

.. math:: \psi_b(1)&=\psi_\textrm{II}(1)\\ \frac{1}{m_b}
    \left(\frac{\partial \psi_b(\xi)}{\partial \xi}\right)_{\xi=1}&=
    \frac{1}{m_\textrm{II}}\left(\frac{\partial \psi_\textrm{II}(\xi)}
    {\partial \xi}\right)_{\xi=1}
    :label: eq_mc6

The equations displayed in :eq:`eq_mc5`-:eq:`eq_mc6` are termed the matching
conditions for current continuity.

Using the nondimensionalized variables of equation :eq:`eq_wf5`, and the
definition of the wave vector
:math:`k^2=2m(\mathcal{E}-U_\textrm{min})\hbar^{-2}`, we can rewrite the
wavefunctions outside of the barrier -- equation :eq:`eq_wf3`

.. math:: \psi_\textrm{I}(\xi)&=\mathfrak{a}_0\exp\left(i\kappa_\textrm{I}\xi
    \right)+\mathfrak{r}\exp\left(-i\kappa_\textrm{I}\xi\right),\quad\xi<0\\
    \psi_\textrm{II}(\xi)&=\mathfrak{t}
    \exp\left(i\kappa_\textrm{II}\xi\right),\quad \xi>1
    :label: eq_mc7

where :math:`\mathfrak{a}_0`, :math:`\mathfrak{r}`, and
:math:`\mathfrak{t}` are respectively the incident amplitude, reflection
amplitude, and transmission amplitude, and where the electron wave vector are

.. math:: \kappa^2_\textrm{I}&=\alpha^2_\textrm{I}
    \left(\eta-\eta_{f,\textrm{I}}\right)\\\kappa^2_\textrm{II}&=
    \alpha^2_\textrm{II}\left(\eta-\eta_\textrm{c, eq}\right)
    :label: eq_mc8

For the holes the wave vector is

.. math:: \kappa^2_\textrm{I}&=\alpha^2_\textrm{I}\left(\eta_{f,\textrm{I}}
    -\eta\right)\\\kappa^2_\textrm{II}&=\alpha^2_\textrm{II}
    \left(\eta_\textrm{v, eq}-\eta\right)
    :label: eq_mc8a

With :math:`\alpha^2_\textrm{I} = 2m_\textrm{I}x_b^2k_BT\hbar^{-2}` and
:math:`\alpha^2_\textrm{I} = 2m_\textrm{II}x_b^2k_BT\hbar^{-2}`.

Substituting equations :eq:`eq_wf13` and :eq:`eq_mc7` in the matching
conditions defined by equations :eq:`eq_mc5`-:eq:`eq_mc6` gives the following
four equations:

.. math:: \mathfrak{a}_0+\mathfrak{r}=\alpha_s\mathcal{U}_b+\beta_s
    \mathcal{V}_b
    :label: eq_mc9

.. math:: \mathfrak{a}_0-\mathfrak{r}=-\Gamma_b\left(\alpha_s\mathcal{U}'_b+
    \beta_s\mathcal{V}'_b\right)
    :label: eq_mc10

..  math:: \alpha_s\mathcal{U}_0+\beta_s\mathcal{V}_0=\tau
    :label: eq_mc11

.. math:: \alpha_s\mathcal{U}'_0+\beta_s\mathcal{V}'_0=
    -\frac{\tau}{\Gamma_0}
    :label: eq_mc12


.. toggle-header::
    :header: see details

        From the chain rule, we have

        .. math:: \frac{\partial}{\partial \xi}\mathcal{U}(-\kappa_s^2,\xi_s)
            =\frac{\partial}{\partial \xi_s}\mathcal{U}(-\kappa_s^2,\xi_s)
            \frac{\partial \xi_s}{\partial \xi}
            :label: eq_mvb1

        and

        .. math:: \frac{\partial}{\partial \xi}\mathcal{V}(-\kappa_s^2,\xi_s)=
            \frac{\partial}{\partial \xi_s}\mathcal{V}(-\kappa_s^2,\xi_s)
            \frac{\partial \xi_s}{\partial \xi}
            :label: eq_mcb2

        where

        .. math:: \frac{\partial \xi_s}{\partial \xi}=-\frac{1}{\lambda_s}
            :label: eq_mcb3

Were :math:`\xi_s(0)=\xi_{s,b}=1/\lambda_s` and :math:`\xi_s(1)=0`.

The specific PCFs are defined by
:math:`\mathcal{U}_b=\mathcal{U}(-\kappa_s^2,\xi_{s,b})`, 
:math:`\mathcal{V}_b=\mathcal{V}(-\kappa_s^2,\xi_{s,b})`, 
:math:`\mathcal{U}_0=\mathcal{U}(-\kappa_s^2,0)`, 
:math:`\mathcal{V}_b=\mathcal{U}(-\kappa_s^2,0)` and their derivatives by:

.. math:: \frac{\partial}{\partial \xi_s}\mathcal{U}(-\kappa_s^2,\xi_s)&=
    \mathcal{U}'\\\frac{\partial}{\partial \xi_s}\mathcal{V}
    (-\kappa_s^2,\xi_s)&=\mathcal{V}'
    :label: eq_mc13

where

.. math:: \Gamma_b=\frac{m_\textrm{I}}{i\kappa_\textrm{I}m_b\lambda_s}=
    \frac{-im_\textrm{I}}{\kappa_\textrm{I}m_b\lambda_s}
    :label: eq_mc14

.. math:: \Gamma_0=\frac{m_\textrm{II}}{i\kappa_\textrm{II}m_b\lambda_s}=
    \frac{-im_\textrm{II}}{\kappa_\textrm{II}m_b\lambda_s}
    :label: eq_mc15

.. math:: \tau=\mathfrak{t}\exp\left(i\kappa_\textrm{II}\right)
    :label: eq_mc16

Using equations :eq:`eq_mc11`-:eq:`eq_mc12`, and the wronskian property of the
PCFs :cite:`a-DLMF`.

.. math:: \mathscr{W}\left\{\mathcal{U}(a,z),\mathcal{V}(a,z)\right\}=
    &\mathcal{U}(a,z)\mathcal{V}'(a,z)-\mathcal{U}'(a,z)\mathcal{V}(a,z)\\=&
    \sqrt{2/\pi}
    :label: eq_mc17

we obtain

.. math:: \alpha_s=&\tau\frac{1}{\mathcal{U}_0}\left(1+\sqrt{\frac{\pi}{2}}
    \left(\mathcal{U}'_0+\frac{\mathcal{U}_0}{\Gamma_0}\right)\right)\\
    \beta_s=&-\tau\left(\mathcal{U}'_0+\frac{\mathcal{U}_0}{\Gamma_0}\right)
    \sqrt{\frac{\pi}{2}}
    :label: eq_mc18

.. toggle-header::
    :header: see details

        From :eq:`eq_mc11` isolate :math:`\alpha_s`

        .. math:: \alpha_s=\frac{\tau-\beta_s\mathcal{V}_0}{\mathcal{U}_0}
            :label: eq_mcc1

        Substituting :eq:`eq_mcc1` in :eq:`eq_mc12` and isolate
        :math:`\beta_s`:

        .. math:: \tau\frac{\mathcal{U}'_0}{\mathcal{U}_0}-
            \beta_s\mathcal{V}_0\frac{\mathcal{U}'_0}{\mathcal{U}_0}+
            \beta_s\mathcal{V}'_0=-\frac{\tau}{\Gamma_0}
            :label: eq_mcc2

        .. math:: \frac{\beta_s}{\mathcal{U}_0}
            \left(\mathcal{V}_0\mathcal{U}'_0-\mathcal{V}'_0\mathcal{U}_0
            \right)=\tau\left(\frac{\mathcal{U}'_0}{\mathcal{U}_0}+\frac{1}
            {\Gamma_0}\right)
            :label: eq_mcc3

        .. math:: \beta_s=\tau\left(\mathcal{U}'_0+\frac{\mathcal{U}_0}
            {\Gamma_0}\right)\left(\mathcal{V}_0\mathcal{U}'_0-
            \mathcal{V}'_0\mathcal{U}_0\right)^{-1}
            :label: eq_mcc4

        Using :eq:`eq_mc17` in :eq:`eq_mcc4`

        .. math:: \beta_s=-\tau\sqrt{\frac{\pi}{2}}\left(\mathcal{U}'_0+
            \frac{\mathcal{U}_0}{\Gamma_0}\right)\left(\mathcal{V}_0
            \mathcal{U}'_0-\mathcal{V}'_0\mathcal{U}_0\right)^{-1}
            :label: eq_mcc5

        Substituting :eq:`eq_mcc5` in :eq:`eq_mcc1` gives

        .. math:: \alpha_s=\tau\frac{1}{\mathcal{U}_0}\left(1+
            \sqrt{\frac{\pi}{2}}\left(\mathcal{U}'_0+\frac{\mathcal{U}_0}
            {\Gamma_0}\right)\right)
            :label: eq_mcc6

Transmission coefficient
========================

Considering the parabolic barrier approximation, it is possible to define the
transmission coefficient analytically using the Parabolic Cylinder Functions
(PCFs) -- see :cite:`a-Schenk1994`.

Here we consider that the transmission coefficient is independent of the
transverse momentum (:math:`k_y`, :math:`k_z`). We also neglect collision in
the barrier, the presence of interfacial layer, and image effects. For sake of
simplicity, we also consider a constant effective mass in the barrier region,
i.e. the effective mass is considered independent of the particles energy.

The quantum transmission coefficient is defined as the ratio of the current
transmitted in region II over incident current in region I. For a wave
traveling in the positive $x$ direction, i.e. from the metal (region I) to the
bulk semiconductor(region II),

.. math:: \mathcal{T}=\frac{|J_\textrm{II}(\xi)|}{|J_\textrm{I}(\xi)|}
    :label: eq_T1

From the following complex property,

.. math:: \mathcal{Z}-\mathcal{Z}^\ast = 2 i\textrm{Im}(\mathcal{Z})
    :label: eq_T2

where :math:`\mathcal{Z}=\psi^\ast(\xi)\frac{\partial}{\partial \xi}\psi(\xi)`,
we can rewrite :eq:`eq_mc1` as:

.. math:: J_\xi(\xi)=q\frac{\hbar x_b}{m}\textrm{Im}\left(\psi^\ast(\xi)
    \frac{\partial}{\partial \xi}\psi(\xi)\right)
    :label: eq_T3

Considering the wave functions defined by :eq:`eq_mc7` we get:

.. math:: \mathcal{T}=\frac{\kappa_\textrm{II}m_\textrm{I}}{\kappa_\textrm{I}
    m_\textrm{II}}\frac{\mathfrak{t}\mathfrak{t}^\ast}{\mathfrak{a}_0
    \mathfrak{a}_0^\ast}
    :label: eq_T4

.. toggle-header::
    :header: see details

        From :eq:`eq_T1`, and :eq:`eq_T3`, we have

        .. math:: \mathcal{T}=\frac{\left|q\frac{\hbar x_b}
            {m_\textrm{II}}\textrm{Im}\left(\psi_\textrm{II}^\ast(\xi)
            \frac{\partial}{\partial \xi}\psi_\textrm{II}(\xi)\right)\right|}
            {\left|q\frac{\hbar x_b}{m_\textrm{I}}\textrm{Im}\left(
            \psi_\textrm{I}^\ast(\xi)\frac{\partial}{\partial \xi}
            \psi_\textrm{I}(\xi)\right)\right|}
            :label: eq_Ta1

        or

        .. math:: \mathcal{T}=\frac{\left|m_\textrm{I}\textrm{Im}
            \left(\psi_\textrm{II}^\ast(\xi)\frac{\partial}{\partial \xi}
            \psi_\textrm{II}(\xi)\right)\right|}
            {\left|m_\textrm{II}\textrm{Im}\left(\psi_\textrm{I}^\ast(\xi)
            \frac{\partial}{\partial \xi}\psi_\textrm{I}(\xi)\right)\right|}
            :label: eq_Ta2

        With the complex property

        .. math:: \left( a\exp\left(i\kappa \xi\right)\right)^\ast =
            a^\ast\exp\left(-i\kappa \xi\right)
            :label: eq_Ta3

        and using :eq:`eq_mc7` in :eq:`eq_Ta2` we get

        .. math:: \mathcal{T}=\frac{\left|m_\textrm{I}\textrm{Im}
            \left(i\kappa_\textrm{II}\mathfrak{t}\mathfrak{t}^\ast\right)
            \right|}
            {\left|m_\textrm{II}\textrm{Im}\left(i\kappa_\textrm{I}
            \mathfrak{a}_0\mathfrak{a}^\ast_0\right)\right|}
            :label: eq_Ta4

        which gives

        .. math:: \mathcal{T}=\frac{\left|m_\textrm{I}\kappa_\textrm{II}
            \mathfrak{t}\mathfrak{t}^\ast\right|}
            {\left|m_\textrm{II}\kappa_\textrm{I}\mathfrak{a}_0
            \mathfrak{a}^\ast_0\right|}
            :label: eq_Ta5

Where :math:`\kappa_\textrm{I}`, :math:`\kappa_\textrm{II}`,
:math:`m_\textrm{I}`, and :math:`m_\textrm{II}` are real values and where

.. math:: \mathfrak{a}_0\mathfrak{a}_0^\ast=\mathfrak{r}\mathfrak{r}^\ast+
    \mathfrak{t}\mathfrak{t}^\ast
    :label: eq_T5

Using equations :eq:`eq_mc9`-:eq:`eq_mc12` and doing some manipulations,
we get the following ratio of amplitudes:

.. math:: \begin{split}\frac{\tau}{\mathfrak{a}_0}=\sqrt{\frac{8}{\pi}}
    \Bigg[\left(\mathcal{U}_b-\Gamma_b\mathcal{U}'_b\right)
    \left(\frac{\mathcal{V}_0}{\Gamma_0}+\mathcal{V}'_0\right)-& \\
    \left(\mathcal{V}_b-\Gamma_b\mathcal{V}'_b\right)
    \left(\frac{\mathcal{U}_0}{\Gamma_0}+\mathcal{U}'_0\right)\Bigg]^{-1}
    \end{split}
    :label: eq_T6

.. toggle-header::
    :header: details

        From :eq:`eq_mc11` isolate :math:`\alpha_s`

        .. math:: \alpha_s = \frac{\tau-\beta_s\mathcal{V}_0}{\mathcal{U}_0}
            :label: eq_Tb1

        Substituting :eq:`eq_Tb1` in :eq:`eq_mc12` and isolate :math:`\beta_s`:

        .. math:: \tau\frac{\mathcal{U}'_0}{\mathcal{U}_0}-
            \beta_s\mathcal{V}_0\frac{\mathcal{U}'_0}{\mathcal{U}_0}+
            \beta_s\mathcal{V}'_0=-\frac{\tau}{\Gamma_0}
            :label: eq_Tb2

        .. math:: \frac{\beta_s}{\mathcal{U}_0}\left(\mathcal{V}_0
            \mathcal{U}'_0-\mathcal{V}'_0\mathcal{U}_0\right)=
            \tau\left(\frac{\mathcal{U}'_0}{\mathcal{U}_0}+\frac{1}{\Gamma_0}
            \right)
            :label: eq_Tb3

        .. math:: \beta_s=\tau\left(\mathcal{U}'_0+\frac{\mathcal{U}_0}
            {\Gamma_0}\right)\left(\mathcal{V}_0\mathcal{U}'_0-
            \mathcal{V}'_0\mathcal{U}_0\right)^{-1}
            :label: eq_Tb4

        Add :eq:`eq_mc9` and :eq:`eq_mc10`

        .. math:: 2\mathfrak{a}_0=\alpha_s\left(\mathcal{U}_b-
            \Gamma_b\mathcal{U}'_b\right)+\beta_s\left(\mathcal{V}_b-
            \Gamma_b\mathcal{V}'_b\right)
            :label: eq_Tb5

        Than substitute :eq:`eq_Tb1` in :eq:`eq_Tb5` and isolate
        :math:`\beta_s`:

        .. math:: 2\mathfrak{a}_0 = \frac{\tau-\beta_s\mathcal{V}_0}
            {\mathcal{U}_0} \left(\mathcal{U}_b-\Gamma_b\mathcal{U}'_b\right)
            +\beta_s\left(\mathcal{V}_b-\Gamma_b\mathcal{V}'_b\right)
            :label: eq_Tb6

        .. math:: \begin{split}2\mathfrak{a}_0=\frac{\tau}{\mathcal{U}_0}
            \left(\mathcal{U}_b-\Gamma_b\mathcal{U}'_b\right)-& \\
            \frac{\beta_s}{\mathcal{U}_0}\left(\mathcal{V}_0
            \left(\mathcal{U}_b-\Gamma_b\mathcal{U}'_b\right)-\mathcal{U}_0
            \left(\mathcal{V}_b-\Gamma_b\mathcal{V}'_b\right)\right)\end{split}
            :label: eq_Tb7

        Which gives

        ..  math:: \beta_s=\frac{\tau\left(\mathcal{U}_b-\Gamma_b
            \mathcal{U}'_b\right)-2\mathfrak{a}_0\mathcal{U}_0}
            {\left(\mathcal{V}_0\left(\mathcal{U}_b-\Gamma_b
            \mathcal{U}'_b\right)-\mathcal{U}_0\left(\mathcal{V}_b-
            \Gamma_b\mathcal{V}'_b\right)\right)}
            :label: eq_Tb8

        Equating :eq:`eq_Tb4` and :eq:`eq_Tb8`

        .. math:: \frac{\tau\left(\mathcal{U}'_0+\frac{\mathcal{U}_0}
            {\Gamma_0}\right)}{\left(\mathcal{V}_0\mathcal{U}'_0-
            \mathcal{V}'_0\mathcal{U}_0\right)}=\frac{\tau
            \left(\mathcal{U}_b-\Gamma_b\mathcal{U}'_b\right)-
            2\mathfrak{a}_0\mathcal{U}_0}{\left(\mathcal{V}_0
            \left(\mathcal{U}_b-\Gamma_b\mathcal{U}'_b\right)-
            \mathcal{U}_0\left(\mathcal{V}_b-\Gamma_b\mathcal{V}'_b\right)
            \right)}

        .. math:: \begin{split}\tau\left(\mathcal{U}'_0+\frac{\mathcal{U}_0}
            {\Gamma_0}\right)\left(\mathcal{V}_0\left(\mathcal{U}_b-
            \Gamma_b\mathcal{U}'_b\right)-\mathcal{U}_0\left(\mathcal{V}_b-
            \Gamma_b\mathcal{V}'_b\right)\right)-& \\ \tau\left(\mathcal{U}_b
            -\Gamma_b\mathcal{U}'_b\right)\left(\mathcal{V}_0\mathcal{U}'_0-
            \mathcal{V}'_0\mathcal{U}_0\right)=-2\mathfrak{a}_0\mathcal{U}_0
            \left(\mathcal{V}_0\mathcal{U}'_0-\mathcal{V}'_0\mathcal{U}_0
            \right)\end{split}

        .. math:: \begin{split}\tau\Bigg(\left(\mathcal{U}_b-
            \Gamma_b\mathcal{U}'_b\right)\bigg(\mathcal{V}_0\left(
            \frac{\mathcal{U}_0}{\Gamma_0}+\mathcal{U}'_0\right)+
            \mathcal{V}'_0\mathcal{U}_0-\mathcal{V}_0\mathcal{U}'_0\bigg)-
            & \\ \left(\frac{\mathcal{U}_0}{\Gamma_0}+\mathcal{U}'_0\right)
            \mathcal{U}_0\left(\mathcal{V}_b-\Gamma_b\mathcal{V}'_b\right)
            \Bigg)=-2\mathfrak{a}_0\mathcal{U}_0\left(\mathcal{V}_0
            \mathcal{U}'_0-\mathcal{V}'_0\mathcal{U}_0\right)\end{split}

        .. math:: \begin{split}\tau\Bigg(\left(\mathcal{U}_b-\Gamma_b
            \mathcal{U}'_b\right)\bigg(\mathcal{U}_0\left(\frac{\mathcal{V}_0}
            {\Gamma_0}+\mathcal{V}'_0\right)\bigg)-& \\ \left(
            \frac{\mathcal{U}_0}{\Gamma_0}+\mathcal{U}'_0\right)\mathcal{U}_0
            \left(\mathcal{V}_b-\Gamma_b\mathcal{V}'_b\right)\Bigg)=
            -2\mathfrak{a}_0\mathcal{U}_0\left(\mathcal{V}_0\mathcal{U}'_0-
            \mathcal{V}'_0\mathcal{U}_0\right)\end{split}

        Using the Wronskian property of the PCFs :eq:`eq_mc17`,

        .. math:: \mathscr{W}\left\{\mathcal{U}(a,z),\mathcal{V}(a,z)\right\}
            =\sqrt{2/\pi}
            :label: eq_Tb9

        we get

        .. math:: \begin{split}\frac{\tau}{\mathfrak{a}_0}=\sqrt{\frac{8}
            {\pi}}\Bigg[\left(\mathcal{U}_b-\Gamma_b\mathcal{U}'_b\right)
            \left(\frac{\mathcal{V}_0}{\Gamma_0}+\mathcal{V}'_0\right)-& \\
            \left(\mathcal{V}_b-\Gamma_b\mathcal{V}'_b\right)
            \left(\frac{\mathcal{U}_0}{\Gamma_0}+\mathcal{U}'_0\right)
            \Bigg]^{-1}\label{eq:Tb10}\end{split}

Using :eq:`eq_T6` in :eq:`eq_T4` gives

.. math:: \mathcal{T}(\eta)=\frac{2}{g(\eta)+1}

.. toggle-header::
    :header: see details

        Substituting the expression for :math:`\Gamma_b` and
        :math:`\Gamma_0` in  the complex product of :eq:`eq_T6` gives

        .. math:: \begin{split}\frac{\mathfrak{a}_0\mathfrak{a}_0^\ast}
            {\mathfrak{t}\mathfrak{t}^\ast}=\frac{\pi}{8}\Bigg|
            \left(\mathcal{U}_b+\frac{im_\textrm{I}}{\kappa_\textrm{I}
            m_b\lambda_s}\mathcal{U}'_b\right)\left(\mathcal{V}'_0+
            \frac{i\kappa_\textrm{II}m_b\lambda_s}{m_\textrm{II}}
            \mathcal{V}_0\right)-& \\ \left(\mathcal{V}_b+\frac{im_\textrm{I}}
            {\kappa_\textrm{I}m_b\lambda_s}\mathcal{V}'_b\right)
            \left(\mathcal{U}'_0+\frac{i\kappa_\textrm{II}m_b\lambda_s}
            {m_\textrm{II}}\mathcal{U}_0\right)\Bigg|^2\end{split}

        where we used the complex property
        :math:`\mathcal{Z}\mathcal{Z}^\ast=\left|\mathcal{Z}\right|^2`.
        Separating the real and imaginary part gives

        .. math:: \begin{split}\frac{\left|\mathfrak{a}_0\right|^2}
            {\left|\mathfrak{t}\right|^2}=\frac{\pi}{8}
            \Bigg|\left(\left(\mathcal{U}_b\mathcal{V}'_0-\mathcal{U}'_0
            \mathcal{V}_b\right)+\frac{m_\textrm{I}\kappa_\textrm{II}}
            {m_\textrm{II}\kappa_\textrm{I}}\left(\mathcal{U}_0\mathcal{V}'_b
            -\mathcal{U}'_b\mathcal{V}_0\right)\right)+& \\
            i\left(\frac{m_\textrm{I}}{\kappa_\textrm{I}m_b\lambda_s}
            \left(\mathcal{U}'_b\mathcal{V}'_0-\mathcal{U}'_0
            \mathcal{V}'_b\right)+\frac{\kappa_\textrm{II}m_b\lambda_s}
            {m_\textrm{II}}\left(\mathcal{V}_0\mathcal{U}_b-\mathcal{U}_0
            \mathcal{V}_b\right)\right)\Bigg|^2\end{split}

        Using the variables

        .. math:: A & = \left(\mathcal{U}_b\mathcal{V}'_0-\mathcal{U}'_0
            \mathcal{V}_b\right)\\ B & = \left(\mathcal{U}_0\mathcal{V}'_b-
            \mathcal{U}'_b\mathcal{V}_0\right)\\ C & = \left(\mathcal{U}'_b
            \mathcal{V}'_0-\mathcal{U}'_0\mathcal{V}'_b\right)\\
            D & = \left(\mathcal{U}_b\mathcal{V}_0-\mathcal{U}_0\mathcal{V}_b
            \right)

        and the definition of the norm of complex numbers, i.e. if
        :math:`\mathcal{Z}=a+ib`,
        :math:`\left|\mathcal{Z}\right|=\sqrt{a^2+b^2}`, we have

        .. math:: \begin{split}\frac{|\mathfrak{t}|^2}{|\mathfrak{a}_0|^2}=
            \frac{8}{\pi}\Bigg[\left(A+\frac{m_\textrm{I}\kappa_\textrm{II}}
            {m_\textrm{II}\kappa_\textrm{I}}B\right)^2+& \\
            \left(\frac{m_\textrm{I}}{\kappa_\textrm{I}m_b\lambda_s}C+
            \frac{\kappa_\textrm{II}m_b\lambda_s}{m_\textrm{II}}D\right)
            ^2\Bigg]^{-1}\end{split}
            :label: eq_Tc1

        Expanding the terms in :eq:`eq_Tc1` we obtain

        .. math:: \begin{split}\frac{|\mathfrak{t}|^2}{|\mathfrak{a}_0|^2}=
            \frac{8}{\pi}\Bigg[A^2+2\frac{m_\textrm{I}\kappa_\textrm{II}}
            {m_\textrm{II}\kappa_\textrm{I}}\left(AB+CD\right)+& \\
            \left(\frac{m_\textrm{I}\kappa_\textrm{II}}{m_\textrm{II}
            \kappa_\textrm{I}}\right)^2B^2+\left(\frac{m_\textrm{I}}
            {\kappa_\textrm{I}m_b\lambda_s}\right)^2C^2+
            \left(\frac{\kappa_\textrm{II}m_b\lambda_s}{m_\textrm{II}}
            \right)^2D^2\Bigg]^{-1}\end{split}

        With

        .. math:: AB+CD=\left(\mathcal{U}_0\mathcal{V}'_0-\mathcal{U}'_0
            \mathcal{V}_0\right)\left(\mathcal{U}_b\mathcal{V}'_b-
            \mathcal{U}'_b\mathcal{V}_b\right)

        and the Wronskian definition -- equation :eq:`eq_mc17`, we have
        :math:`AB+CD=2/\pi` and

        .. math:: \begin{split}\frac{|\mathfrak{t}|^2}{|\mathfrak{a}_0|^2}=
            \frac{8}{\pi}\Bigg[A^2+\left(\frac{m_\textrm{I}\kappa_\textrm{II}}
            {m_\textrm{II}\kappa_\textrm{I}}\right)^2B^2+& \\
            \left(\frac{m_\textrm{I}}{\kappa_\textrm{I}m_b\lambda_s}\right)^2
            C^2+\left(\frac{\kappa_\textrm{II}m_b\lambda_s}{m_\textrm{II}}
            \right)^2D^2+\frac{4}{\pi}\frac{m_\textrm{I}\kappa_\textrm{II}}
            {m_\textrm{II}\kappa_\textrm{I}}\Bigg]^{-1}\end{split}

        Multiplying both side of the previous equation by
        :math:`\frac{m_\textrm{I}\kappa_\textrm{II}}{m_\textrm{II}\kappa_\textrm{I}}`
        we obtain the transmission coefficient

        .. math:: \begin{split}\mathcal{T}=\frac{8}{\pi}\Bigg[
            \frac{m_\textrm{II}\kappa_\textrm{I}}{m_\textrm{I}
            \kappa_\textrm{II}}A^2+\frac{m_\textrm{I}\kappa_\textrm{II}}
            {m_\textrm{II}\kappa_\textrm{I}}B^2+& \\
            \frac{m_\textrm{I}m_\textrm{II}}{\kappa_\textrm{I}
            \kappa_\textrm{II}m_b^2\lambda_s^2}C^2+\frac{\kappa_\textrm{I}
            \kappa_\textrm{II}m_b^2\lambda_s^2}{m_\textrm{I}m_\textrm{II}}D^2+
            \frac{4}{\pi}\Bigg]^{-1}\end{split}

        or

        .. math:: \begin{split}\mathcal{T}=2\Bigg[\frac{\pi}{4}
            \bigg(\frac{m_\textrm{II}\kappa_\textrm{I}}{m_\textrm{I}
            \kappa_\textrm{II}}A^2+\frac{m_\textrm{I}\kappa_\textrm{II}}
            {m_\textrm{II}\kappa_\textrm{I}}B^2+& \\ \frac{m_\textrm{I}
            m_\textrm{II}}{\kappa_\textrm{I}\kappa_\textrm{II}m_b^2
            \lambda_s^2}C^2+\frac{\kappa_\textrm{I}\kappa_\textrm{II}m_b^2
            \lambda_s^2}{m_\textrm{I}m_\textrm{II}}D^2\bigg)+1\Bigg]^{-1}
            \end{split}

where

.. math:: \begin{split}g=\frac{\pi}{4}\bigg(\frac{m_\textrm{II}
    \kappa_\textrm{I}}{m_\textrm{I}\kappa_\textrm{II}}A^2+\frac{m_\textrm{I}
    \kappa_\textrm{II}}{m_\textrm{II}\kappa_\textrm{I}}B^2+\frac{m_\textrm{I}
    m_\textrm{II}}{\kappa_\textrm{I}\kappa_\textrm{II}m_b^2\lambda_s^2}C^2+&
    \\ \frac{\kappa_\textrm{I}\kappa_\textrm{II}m_b^2\lambda_s^2}
    {m_\textrm{I}m_\textrm{II}}D^2\bigg)\end{split}

and where

.. math:: A&=\left(\mathcal{U}_b\mathcal{V}'_0-\mathcal{U}'_0\mathcal{V}_b
    \right)\\ B&=\left(\mathcal{U}_0\mathcal{V}'_b-\mathcal{U}'_b
    \mathcal{V}_0\right)\\ C&=\left(\mathcal{U}'_b\mathcal{V}'_0-
    \mathcal{U}'_0\mathcal{V}'_b\right)\\ D&=\left(\mathcal{U}_b\mathcal{V}_0-
    \mathcal{U}_0\mathcal{V}_b\right)

When the barrier length becomes negligible, we can see that the transmission
coefficient tends to

.. math:: \lim_{x_b\rightarrow 0}\mathcal{T}=
  \frac{2}{\left(\frac{m_\textrm{I}\kappa_\textrm{I}}
  {m_\textrm{II}\kappa_\textrm{II}}+
  \frac{m_\textrm{II}\kappa_\textrm{II}}{m_\textrm{I}
  \kappa_\textrm{I}}\right)+1}

.. raw:: html

    <video width="600" height="450" align="center" controls loop>
    <source src="_static/TCoef.mp4" type="video/mp4">
    </video>

.. _animation4:

`Animation 4: Electron and hole transmission coefficients as a function of
the particle normalized energy :math:`\eta` for a n-type semiconductor. For
this animation the electron effective mass is equal in all regions and the hole
mass as well.`


Current Densities
=================

The emission current at a semiconductor-metal contact can be calculated
considering the perpendicular (to the surface) current conservation between
the metal and the bulk semiconductor.

The current density is expressed as a function of the wave vector by:

.. math:: J(\mathbf{k})=J_n(\mathbf{k})+J_p(\mathbf{k})

where :math:`J_n(\mathbf{k})` and :math:`J_p(\mathbf{k})` are respectively the
electron and hole current density given by:

.. math:: \begin{split}J_n(\mathbf{k})=-q\int_{k_{x\textrm{min},e}}^\infty
    v_x(k_x)\mathcal{T}_e(k_x)& \\ \left[\int_0^\infty\int_0^\infty
    n(\mathbf{k})dk_ydk_z\right]dk_x\end{split}
    :label: eq_cd1a

and

.. math:: \begin{split}J_p(\mathbf{k})=q\int_{-\infty}^{k_{x\textrm{max},h}}
     v_x(k_x)\mathcal{T}_h(k_x)& \\ \left[\int_{-\infty}^0\int_{-\infty}^0
     p(\mathbf{k})dk_ydk_z\right]dk_x \end{split}
    :label: eq_cd1b

Where :math:`v_x` is the particle velocity, and :math:`k_{x\textrm{min},e}`
and :math:`k_{x\textrm{max},h}` are the minimum and maximum wave vector
amplitude for electrons and holes.  

:math:`\mathcal{T}_e(k_x)` and :math:`\mathcal{T}_h(k_x)` are the transmission
coefficient for electrons and holes, and :math:`n(\mathbf{k})` and
:math:`p(\mathbf{k})` are the electron and hole number densities.
Note that by convention, the electrons travels in the direction opposite to
the current.

Since we assume that the total current density has only one spatial component
along the :math:`x` direction, the transverse components of the wave vector is
unaltered by the process and is only needed to evaluate the number densities
:math:`n(\mathbf{k})` and :math:`p(\mathbf{k})`.

To simplify the analysis, we define the wave vector
:math:`\mathbf{k}=k_x\mathbf{\hat{i}}+k_y\mathbf{\mathbf{\hat{j}}}+k_x\mathbf{\hat{k}}`
in cylindrical coordinates, i.e.

.. math:: k_x& =k_x \\ k_y& =k_r\cos(\theta) \\
    k_z& =k_r\sin(\theta)
    :label: eq_dc2

Where :math:`k_r=\sqrt{k_y^2+k_z^2}` and where the polar angle
:math:`\theta=\textrm{atan}(k_z/k_y)`.

Electron Current Density
------------------------

In our 1D model the net flow of tunneling electron current is obtained summing
the contribution of electrons coming from region I to region II and
`vice versa`:

.. math:: J_n=J_n^{\textrm{I}\rightarrow\textrm{II}}-J_n^{\textrm{I}
    \rightarrow\textrm{II}}
    :label: eq_cd2a

Using :eq:`eq_cd1a`, in cylindrical coordinates we can see that

.. math:: \begin{split}J_n(\mathbf{k})=-q\int_{k_{x\textrm{min}}}^\infty v_x
    (k_x)\mathcal{T}(k_x)& \\ \left[\int_0^{2\pi}\int_0^\infty
    \left(n_\textrm{I}(\mathbf{k})-n_\textrm{II}(\mathbf{k})\right)k_rdk_r
    d\theta\right]dk_x\end{split}
    :label: eq_cd3

Where the electron number densities are expressed in terms of the
Fermi-Dirac distribution :math:`f(\mathbf{k})`

.. math:: \left(n_\textrm{I}(\mathbf{k})-n_\textrm{II}(\mathbf{k})\right)=
    g(\mathbf{k})\left(f_\textrm{I}(\mathbf{k})-f_\textrm{II}(\mathbf{k})
    \right)
    :label: eq_cd4

where :math:`g(\mathbf{k})` is the density of states in the reciprocal
(momentum) space:

.. math:: g(\mathbf{k})=\frac{2}{(2\pi)^3}
    :label: eq_cd7

where the factor 2 in the numerator represent the two possible spins that a
state can hold. The Fermi-Dirac distributions are expressed by:

.. math:: f_\textrm{I}(\mathbf{k})=\frac{1}{1+\exp\left(\eta_r(k_r)+
    \eta_x(k_x)-\eta_{f\textrm{I}}\right)}
    :label: eq_cd5

and

.. math:: f_\textrm{II}(\mathbf{k})=\frac{1}{1+\exp\left(\eta_r(k_r)+
    \eta_x(k_x)-\eta_{f\textrm{II}}\right)}
    :label: eq_cd6

where :math:`\eta = \hbar^2k^2/(2m_ek_BT)`, and where
:math:`\eta_{f\textrm{I}}` and :math:`\eta_{f\textrm{II}}` respectively refer
to the metal and bulk semiconductor Fermi level.

Using :eq:`eq_cd7` in :eq:`eq_cd3` we get

.. math:: \begin{split}J_n(\mathbf{k})=-\frac{q}{4\pi^3}
    \int_{k_{x\textrm{min},e}}^\infty v_x(k_x)\mathcal{T}(k_x)& \\
    \left[\int_0^\infty \left(f_\textrm{I}(\mathbf{k})-
    f_\textrm{II}(\mathbf{k})\right)k_rdk_r\int_0^{2\pi}d\theta\right]dk_x
    \end{split}
    :label: eq_cd8

Since none of the terms in :eq:`eq_cd8` depends on :math:`\theta`, we can
perform the integration over the polar angle and get

.. math:: \begin{split}J_n(\mathbf{k})=-\frac{q}{2\pi^2}
    \int_{k_{x\textrm{min},e}}^\infty v_x(k_x)\mathcal{T}(k_x)& \\
    \left[\int_0^\infty \left(f_\textrm{I}(\mathbf{k})-
    f_\textrm{II}(\mathbf{k})\right)k_rdk_r\right]dk_x\end{split}
    :label: eq_cd9

The electron supply function :math:`S_e(\eta_x)` is defined as

.. math:: S_e(\eta_x)=& \frac{\hbar^2}{m_ek_BT}\int_0^\infty
    \left(f_\textrm{I}(\mathbf{k})-f_\textrm{II}(\mathbf{k})\right)k_rdk_r\\
    =& \textrm{ln}\left(\frac{\left(1+\exp\left(-\eta_x-\eta_{f\textrm{I}}
    \right)\right)}{\left(1+\exp\left(-\eta_x-\eta_{f\textrm{II}}\right)
    \right)}\right)
    :label: eq_cd10

.. toggle-header::
    :header: see details

        With

        .. math:: \eta_r(k_r) = \hbar^2k_r^2/(2m_ek_BT)

        we have

        .. math:: \partial\eta_r = \hbar^2k_r/(mk_BT)\partial k_r.

        Replacing these expressions in :eq:`eq_cd10` we get

        .. math:: S_e(\eta_x)= \int_0^\infty \left(f_\textrm{I}(\eta_r)-
            f_\textrm{II}(\eta_r)\right)d\eta_r

        Using the definition of the Fermi-Dirac distributions :eq:`eq_cd5` and
        :eq:`eq_cd6` we get

        .. math:: S_e(\eta_x)= \int_0^\infty \left(
            \frac{1}{1+\exp\left(\eta_r-a(x)\right)}-
            \frac{1}{1+\exp\left(\eta_r-b(x)\right)}
            \right) d\eta_r


        where :math:`a(x) = \eta_{f\textrm{I}}-\eta_x` and 
        :math:`b(x) = \eta_{f\textrm{II}}-\eta_x`.

        With the change of variable :math:`\zeta_\textrm{I} =\eta_r-a(x)` and 
        :math:`\zeta_\textrm{II} =\eta_r-b(x)`, we get

        .. math:: S_e(\eta_x)= \int_{-a(x)}^\infty
            \frac{1}{1+\exp\left(\zeta_\textrm{I}\right)}d\zeta_\textrm{I}-
            \int_{-b(x)}^\infty
            \frac{1}{1+\exp\left(\zeta_\textrm{II}\right)}
            d\zeta_\textrm{II}

        With the following property:

        .. math:: \int_{-a(x)}^\infty \frac{1}{1+\exp\left(\zeta_\textrm{I}
            \right)}d\zeta_\textrm{I}=& \left[\zeta_\textrm{I}-
            \textrm{ln}\left(\exp(\zeta_\textrm{I})+1\right)\right]_{-a(x)}
            ^\infty\\ =&\left[\textrm{ln}\left(\frac{\exp
            \left(\zeta_\textrm{I}\right)}{\exp\left(\zeta_\textrm{I}\right)+
            1}\right)\right]_{-a(x)}^\infty

        Using L'Hospital rule, we have

        .. math:: \lim_{\zeta_\textrm{I}\rightarrow\infty}
            \frac{\exp\left(\zeta_\textrm{I}\right)}
            {\exp\left(\zeta_\textrm{I}\right)+1} = 1

        and then

        .. math:: \int_{0}^\infty \frac{1}{1+\exp\left(\eta_r-a(x)\right)}
            d\eta_r& =-\textrm{ln}\left(\frac{\exp\left(a(x)\right)}
            {\exp\left(a(x)\right)+1}\right)\\ &=-\textrm{ln}\left(\frac{1}
            {1+\exp\left(\eta_x-\eta_{f\textrm{I}}\right)}\right)

        Similarly we obtain

        .. math:: \begin{split}\int_{0}^\infty \frac{1}{1+\exp\left(\eta_r-
            b(x)\right)}d\eta_r=& \\-\textrm{ln}\left(\frac{1}{1+\exp\left(
            \eta_x-\eta_{f\textrm{II}}\right)}\right)\end{split}

        and

        .. math:: S_e(\eta_x)= \textrm{ln}\left(
            \frac{\left(1+\exp\left(-\eta_x-\eta_\textrm{I,min}\right)\right)}
            {\left(1+\exp\left(-\eta_x-\eta_\textrm{II,min}\right)\right)}
            \right)

Substituting :eq:`eq_cd10` in :eq:`eq_cd9`

.. math:: J_n(k_x)=\frac{m_eqk_BT}{2\pi^2\hbar^2}\int_{k_{x\textrm{min},e}}
    ^\infty v_x(k_x)\mathcal{T}_e(k_x)S_e(k_x)dk_x
    :label: eq_cd11

With :math:`v_xdk_x = k_BT/\hbar\partial \eta_x`, we have

.. math:: J_n(\eta)=\frac{m_eq(k_BT)^2}{2\pi^2\hbar^3}\int_{\eta_\textrm{min}}
    ^\infty \mathcal{T}_e(\eta)S_e(\eta)d\eta
    :label: eq_cd12

where :math:`\eta` refers here to :math:`\eta_x` and :math:`\eta_\textrm{min}`
to :math:`\eta_{c,\textrm{eq}}`.

Holes Current Density
----------------------

Similarly the hole current density is

.. math:: J_p(\eta)=\frac{m_hq(k_BT)^2}{2\pi^2\hbar^3}
    \int_{\eta_{-\infty}}^\textrm{max} \mathcal{T}_h(\eta)S_h(\eta)d\eta
    :label: eq_cd13

where :math:`\eta_\textrm{max}` refers to :math:`\eta_{v,\textrm{eq}}`

.. math:: S_h(\eta)= \textrm{ln}\left(
    \frac{\left(1+\exp\left(\eta_{f\textrm{II}}-\eta\right)\right)}
    {\left(1+\exp\left(\eta_{f\textrm{I}}-\eta\right)\right)}
    \right)
    :label: eq_cd14

.. toggle-header::
    :header: details

        The Fermi-Dirac distribution for the holes is simply the complement of
        the electron distribution:

        .. math:: p(\mathbf{k})=g(\mathbf{k})\left(1-f(\mathbf{k})\right)
            :label: eq_cdb1

        Using :eq:`eq_cdb1` we have

        .. math:: p_\textrm{I}(\mathbf{k})-p_\textrm{II}(\mathbf{k})& =
            g(\mathbf{k})\left(\left(1-f_\textrm{I}(\mathbf{k})\right)-
            \left(1-f_\textrm{II}(\mathbf{k})\right)\right)\\ &=
            g(\mathbf{k})\left(f_\textrm{II}(\mathbf{k})-
            f_\textrm{I}(\mathbf{k})\right)

Figure 2 shows the current density in a n-type semiconductor under different
doping concentrations.


.. _figure2:

.. figure:: _static/figJ.png
   :figwidth: 100%
   :alt: current density

   `Figure 2: Current density as a function of the applied voltage for a
   n-type semiconductor under different doping concentrations. For low doping
   concentrations, the contact approaches the ideal Schottky contact
   (thermionic emission). Under large doping the current shows a ohmic
   behavior.`


References
==========

.. bibliography:: _static/references.bib
    :labelprefix: A
    :keyprefix: a-


Symbols
=======

.. _metal_semi_symbols:
.. csv-table:: List of Symbols
   :header: "Symbol", "Definition", "SI Units"
   :widths: 5, 10, 5

   ":math:`x_b`", "Length of the potential barrier.", "m"
   ":math:`\rho`", "Charge density.", "C/m\ :sup:`3`\ "
   ":math:`\phi_{b,e}`", "Barrier height, electrons.", "eV"
   ":math:`\Phi`", "Metal work function.", "eV"
   ":math:`\chi`", "Electron affinity.", "eV"
   ":math:`E_g`", "Band gap energy.", "eV"
   ":math:`\phi_{b,h}`", "Barrier height, holes.", "eV"
   ":math:`q`", "Elementary charge.", "C"
   ":math:`p`", "Number density, holes.", "m\ :sup:`-3`\ "
   ":math:`n`", "Number density, electrons.", "m\ :sup:`-3`\ "
   ":math:`N_d`", "Number density, donors.", "m\ :sup:`-3`\ "
   ":math:`N_a`", "Number density, acceptors.", "m\ :sup:`-3`\ "
   ":math:`V`", "Electric potential.", "V"
   ":math:`\epsilon_0`", "Permittivity, vacuum.", "F/m"
   ":math:`\epsilon_r`", "Relative permittivity, semiconductor", "1"
   ":math:`\mathbf{E}`", "Electric field.", "V/m"
   ":math:`V_\textrm{bi}`", "Built-in potential.", "V"
   ":math:`V_a`", "Applied potential.", "V"
   ":math:`\mathcal{E}_{f,\textrm{I}}`", "Fermi level, metal.", "eV"
   ":math:`\mathcal{E}_{f,\textrm{I}}`", "Fermi level, semiconductor.", "eV"
   ":math:`k_B`", "Boltzmann constant.", "m\ :sup:`2`\ kg/s\ :sup:`2`\ /K"
   ":math:`T`", "Lattice temperature", "K"
   ":math:`\lambda_D`", "Debye length.", "m"
   ":math:`\psi`", "Wavefunction", "1"
   ":math:`m`", "Effective mass.", "kg"
   ":math:`U`", "Potential energy", "eV"
   ":math:`\hbar`", "Planck constant.", "m\ :sup:`2`\ kg/s"
   ":math:`\Lambda`", "Energy of the system.", "eV"
   ":math:`\omega`", "Angular frequency.", "rad/s"


Code Documentation
==================

The documentation for the code used to generate the figures is available below.

.. toctree::
    :maxdepth: 2

    packages/metal_semi

To access the code clone or download the bitbucket repository
:repo:`metal_semi`.
