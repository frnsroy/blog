# -*- coding: utf-8 -*-
"""
blog.__init__.py
September 11, 2018
@author Francois Roy
"""
import os
from unipath import Path


ROOT_DIR = Path(os.path.abspath(__file__)).ancestor(2)
STATIC_DIR = ROOT_DIR.child('blog').child('_static')


__version__ = '0.0.1'
__import__('pkg_resources').declare_namespace(__name__)