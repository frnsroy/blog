"""
blog.files_metal_semi.py
Francois Roy
January 25, 2019
"""
import os
import scipy.constants as cst
from scipy import integrate
import numpy as np
import matplotlib.patches as patches
import matplotlib as mpl
mpl.rcParams['backend'] = 'TkAgg'
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import inspect
import progressbar
import time
import subprocess

from blog import STATIC_DIR
from metal_semi.contact.barrier import Barrier
from metal_semi.contact.wave import Wave, M_E
from metal_semi.contact import EPSILON_0, Q

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('text', usetex=True)
plt.rc('xtick', labelsize=24)
plt.rc('ytick', labelsize=24)
plt.rc('axes', labelsize=24)


def gen_mp4(anim_name):
    r"""

    :param anim_name:
    """
    # Command to generate the animation with ffmpeg
    cmds = ['ffmpeg', '-y', '-r', '30', '-s', '800x600', '-i',
            STATIC_DIR +'/' + anim_name + '/fig_%d.png',
            STATIC_DIR +'/' + anim_name + '/' + anim_name + '.mp4']
    subprocess.check_call(cmds)


def compute_se(eta, b):
    r"""

    :param eta:
    :param b:
    :return:
    """
    eta_fi = b.values['eta_fI']
    eta_fii = b.values['eta_fs']
    return np.log((1.0+np.exp(-(eta-eta_fi)))/(1.0+np.exp(-(eta-eta_fii))))


def compute_sh(eta, b):
    r"""

    :param eta:
    :param b:
    :return:
    """
    eta_fi = b.values['eta_fI']
    eta_fii = b.values['eta_fs']
    return np.log((1.0+np.exp((eta-eta_fii)))/(1.0+np.exp((eta-eta_fi))))


def get_transmission_coefficient(w, eta, b):
    r"""

    :param eta:
    :param b:
    :return:
    """
    #
    eta_min = np.maximum(b.values['eta_ceq'], b.values['eta_fI'])
    eta_max = np.minimum(b.values['eta_veq'], b.values['eta_fI'])
    te = 0.0
    th = 0.0
    if b.values['xb_e'] > 0.0:
        if eta <= b.values['eta_veq']:
            th = 1.0
        if eta >= eta_min:
            w.set_eta(eta)
            te = w.compute_TCoef('AiryInterpolation')
        else:
            te = 0.0
    else:
        if eta >= b.values['eta_ceq']:
            te = 1.0
        if eta <= eta_max:
            w.set_eta(eta)
            th = w.compute_TCoef('AiryInterpolation')
        else:
            th = 0.0
    return [te, th]


def anim_contact(n=99, n_x1=99):
    """Generate the contact animation

    :param n:
    :param n_x1:
    :return:
    """
    fct_name = inspect.stack()[0][3]
    if not os.path.isdir(STATIC_DIR.child(fct_name)):
        os.mkdir(STATIC_DIR.child(fct_name))
    if os.path.exists(STATIC_DIR.child(fct_name+'.mp4')):
        os.remove(STATIC_DIR.child(fct_name+'.mp4'))
    bar = Barrier(Nd=1e25)
    chi = np.linspace(0.0, 1.0, n)
    chis = np.linspace(1.0, 1.4, n)
    d = 0.1
    x1a = np.linspace(0.0, d, n_x1)

    dpi = 96
    fig = plt.figure(figsize=(878 / dpi, 625 / dpi), dpi=dpi)
    ax = fig.add_subplot(111)
    for i in progressbar.progressbar(range(0, n_x1)):
        time.sleep(0.02)
        x1 = x1a[n_x1 - i - 1]
        phi_b_e0 = bar.values['PhiB_e']
        phi_b_e = phi_b_e0 - (phi_b_e0 - bar.values['Ec_eq']) / d * x1
        v_bi = phi_b_e - bar.values['Ec_eq']
        xb_e = np.sqrt(2 * bar.inputs['epsilonrs'] * EPSILON_0 /
                       (Q * np.abs(bar.values['Ndoping'])) * v_bi)
        eta_c = bar.values['eta_ceq'] + xb_e ** 2 / (
                2 * bar.values['lambda_D'] ** 2) * (1 - (chi - x1)) ** 2
        eta_v = eta_c - bar.values['eta_g']
        ax.set_xlim([-0.3, 1.3])
        ax.set_ylim([-1.5, 1.0])
        xfill = np.append(chi + x1, chis + x1)
        yfillc = np.append(eta_c * bar.values['Vth'],
                           np.ones(n) * eta_c[n - 1] * bar.values['Vth'])
        yfillv = np.append(eta_v * bar.values['Vth'],
                           np.ones(n) * eta_v[n - 1] * bar.values['Vth'])

        ax.fill_between(xfill, yfillc, 2.0, hatch='o', color='none',
                        edgecolor='black', linewidth=2.0)
        ax.fill_between(xfill, -2.0, yfillv, hatch='\\', color='none',
                        edgecolor='black', linewidth=2.0)

        ax.fill_between(np.linspace(-0.4, 0, 11), np.zeros(11),
                        2.0, hatch='o', color='none',
                        edgecolor='black', linewidth=2.0)

        ax.fill_between(np.linspace(-0.4, 0, 11), -2.0,
                        np.zeros(11), hatch='\\', color='none',
                        edgecolor='black', linewidth=2.0)

        ax.plot(chi + x1, eta_c * bar.values['Vth'], '-b',
                chi + x1, eta_v * bar.values['Vth'], '-g',
                chis + x1,
                np.ones(n) * eta_c[n - 1] * bar.values['Vth'], '-b',
                chis + x1,
                np.ones(n) * eta_v[n - 1] * bar.values['Vth'], '-g',
                linewidth=2.0,
                )
        if i == n_x1 - 1:
            ax.plot(np.zeros(n), np.linspace(-2.0, 2.0, n), 'm',
                    linewidth=3.0, )
            ax.arrow(0, 0.5, 0.3, 0.0, head_width=0.1,
                     head_length=0.05, linewidth=2.5, fc='m', ec='m')
            ax.annotate('', xy=(0.0, -0.2), xytext=(1.0, -0.2),
                        arrowprops=dict(arrowstyle='<->', facecolor='magenta'))
            ax.annotate('Barrier Region', xy=(0.0, -0.2),
                        xytext=(0.2, -0.2),
                        bbox=dict(fc="w", linewidth=0.0), fontsize=20)
            ax.add_patch(
                    patches.Rectangle(
                        (0.0, -2.0),  # (x,y)
                        1.0,  # width
                        4.0,  # height
                        facecolor="black",
                        edgecolor="black",
                        linewidth=2.0,
                        linestyle='dashed',
                        fill=False
                    )
                )
        plt.xlabel(r'$\xi=x/x_b$', fontsize=20, color='black')
        plt.ylabel('Energy (eV)', fontsize=20, color='black')
        fig.savefig(STATIC_DIR+'/'+fct_name+'/fig_' + str(i) + '.png',
                    dpi=dpi, bbox_inches='tight')
        plt.cla()
    plt.close(fig)

    # Command to generate 6 copies of the last figure
    for i in range(0, 5):
        cmds = ['cp',
                STATIC_DIR + '/' + fct_name + '/fig_' + str(n_x1 - 1) +
                '.png',
                STATIC_DIR + '/' + fct_name + '/fig_' + str(n_x1 + i) +
                '.png']
        subprocess.call(cmds)

    # Command to generate the animation with ffmpeg
    cmds = ['ffmpeg', '-y', '-r', '30', '-s', '800x600', '-i',
            STATIC_DIR+'/'+fct_name+'/fig_%d.png',
            STATIC_DIR+'/'+fct_name+'/'+fct_name+'.mp4']

    subprocess.check_call(cmds)


def anim_p():
    r"""

    :return:
    """
    fct_name = inspect.stack()[0][3]
    if not os.path.isdir(STATIC_DIR.child(fct_name)):
        os.mkdir(STATIC_DIR.child(fct_name))
    if os.path.exists(STATIC_DIR.child(fct_name + '.mp4')):
        os.remove(STATIC_DIR.child(fct_name + '.mp4'))
    dpi = 96
    fig = plt.figure(figsize=(883/dpi, 642/dpi), dpi=dpi)
    ax1 = fig.add_subplot(111)  # , aspect='equal')
    nd = 0.0
    na = 1.0e24
    bar = Barrier(Nd=nd, Na=na)
    eta_ceq = bar.values['eta_ceq']
    eta_veq = bar.values['eta_veq']
    eta_fs = bar.values['eta_fs']
    ubi = bar.values['ubi']
    vth = bar.values['Vth']
    num_m = 10
    num = 11
    num_s = 10
    num_va = 101
    v_a = np.linspace(-1.0, 1.0, num_va)
    xi_s = np.linspace(1.0, 1.2, num_s)
    xi_m = np.linspace(-0.2, 0.0, num_m)
    ubi_m = -np.ones(num_m)*ubi
    # In the bulk semiconductor
    eta_cs = np.ones(num_s)*eta_ceq
    eta_vs = np.ones(num_s)*eta_veq
    eta_fss = np.ones(num_s)*eta_fs
    y_frame_max = 3.0
    for i in progressbar.progressbar(range(0, num_va)):
        time.sleep(0.02)
        bar.set_applied_voltage(v_a[i])
        ua = bar.values['ua']
        eta_fm = bar.values['eta_fI']
        # In the Metal
        eta_cm = np.ones(num_m)*eta_fm
        # In the Barrier
        levels = bar.barrier_levels(num)
        xi = levels[0]
        eta_c = levels[1]
        eta_v = levels[2]
        # Extend beyound the barrier
        xi = np.append(-0.0001, xi)
        eta_c = np.append(eta_fm, eta_c)
        eta_v = np.append(eta_fm, eta_v)
        xb_e = bar.values['xb_e']
        xb_h = bar.values['xb_h']
        if -eta_fm < ubi:
            xb = xb_e
            sign = 1.0
        else:
            xb = xb_h
            sign = -1.0
        ax1.annotate('I', xy=(-0.125, y_frame_max),
                     xytext=(-0.125, y_frame_max),fontsize=20, family='serif')
        ax1.annotate('Barrier Region', xy=(0.025, y_frame_max),
                     xytext=(0.025, y_frame_max), fontsize=20, family='serif')
        ax1.annotate('II', xy=(1.1, y_frame_max),
                     xytext=(1.1, y_frame_max), fontsize=20, family='serif')
        ax1.annotate(r'$V_a =$ {0: .2f} V'.format(ua*vth), xy=(0.55, -1.8),
                     xytext=(0.55, -1.8), fontsize=20, family='serif')
        ax1.annotate(r'$x_{b,e}=$'+'{: 04.2f} nm'.format(xb_e*1e9),
                     xy=(0.52, -2.2), xytext=(0.52, -2.2), fontsize=20,
                     family='serif')
        ax1.annotate(r'$x_{b,h}=$'+'{: 04.2f} nm'.format(xb_h*1e9),
                     xy=(0.52, -2.6), xytext=(0.52, -2.6), fontsize=20,
                     family='serif')
        ax1.plot(xi_m, eta_cm*vth, 'k--',
                 xi_m, ubi_m*vth, 'm--',
                 xi, eta_c*vth, 'b--',
                 xi, eta_v*vth, 'g--',
                 xi_s, eta_cs*vth, 'b--',
                 xi_s, eta_vs*vth, 'g--',
                 xi_s, eta_fss*vth, 'k',
                 lw=4)
        y_min = -3.0
        y_max = 3.5
        height  = -y_min+y_max
        ax1.set_xlim([-0.2,1.2])
        ax1.set_ylim([y_min,y_max])
        plt.xlabel(r'$\xi=x/x_b$', fontsize=20, color='black')
        plt.ylabel('Energy (eV)', fontsize=20, color='black')
        ax1.add_patch(
            patches.Rectangle(
                (0.0, y_min),   # (x,y)
                1.0,          # width
                height,          # height
                facecolor="black",
                alpha=0.1,
                linewidth=0,
                )
        )
        fig.savefig(STATIC_DIR+'/'+fct_name+'/fig_' + str(i) + '.png',
                    dpi=dpi, bbox_inches='tight')
        plt.cla()
    plt.close(fig)
    gen_mp4(fct_name)


def anim_n():
    r"""

    """
    fct_name = inspect.stack()[0][3]
    if not os.path.isdir(STATIC_DIR.child(fct_name)):
        os.mkdir(STATIC_DIR.child(fct_name))
    if os.path.exists(STATIC_DIR.child(fct_name + '.mp4')):
        os.remove(STATIC_DIR.child(fct_name + '.mp4'))
    dpi = 96
    fig = plt.figure(figsize=(883 / dpi, 642 / dpi), dpi=dpi)
    ax1 = fig.add_subplot(111)  # , aspect='equal')
    nd = 1.0e24
    na = 0.0
    bar = Barrier(Nd=nd, Na=na)
    eta_ceq = bar.values['eta_ceq']
    eta_veq = bar.values['eta_veq']
    eta_fs = bar.values['eta_fs']
    ubi = bar.values['ubi']
    vth = bar.values['Vth']
    num_m = 10
    num = 11
    num_s = 10
    num_va = 101
    v_a = np.linspace(-1.0, 1.0, num_va)
    xi_s = np.linspace(1.0, 1.2, num_s)
    xi_m = np.linspace(-0.2, 0.0, num_m)
    ubi_m = -np.ones(num_m) * ubi
    # In the bulk semiconductor
    eta_cs = np.ones(num_s) * eta_ceq
    eta_vs = np.ones(num_s) * eta_veq
    eta_fss = np.ones(num_s) * eta_fs
    y_frame_max = 3.0
    for i in progressbar.progressbar(range(0, num_va)):
        time.sleep(0.02)
        bar.set_applied_voltage(v_a[i])
        ua = bar.values['ua']
        eta_fm = bar.values['eta_fI']
        # In the Metal
        eta_cm = np.ones(num_m) * eta_fm
        # In the Barrier
        levels = bar.barrier_levels(num)
        xi = levels[0]
        eta_c = levels[1]
        eta_v = levels[2]
        # Extend beyound the barrier
        xi = np.append(-0.0001, xi)
        eta_c = np.append(eta_fm, eta_c)
        eta_v = np.append(eta_fm, eta_v)
        xb_e = bar.values['xb_e']
        xb_h = bar.values['xb_h']
        if -eta_fm < ubi:
            xb = xb_e
            sign = 1.0
        else:
            xb = xb_h
            sign = -1.0
        ax1.annotate('I', xy=(-0.125, y_frame_max), xytext=(-0.125, y_frame_max),
                     fontsize=20, family='serif')
        ax1.annotate('Barrier Region', xy=(0.025, y_frame_max),
                     xytext=(0.025, y_frame_max),
                     fontsize=20, family='serif')
        ax1.annotate('II', xy=(1.1, y_frame_max), xytext=(1.1, y_frame_max),
                     fontsize=20, family='serif')
        ax1.annotate(r'$V_a =$ {0: .2f} V'.format(ua * vth), xy=(0.55, -1.8),
                     xytext=(0.55, -1.8),
                     fontsize=20, family='serif')
        ax1.annotate(r'$x_{b,e}=$' + '{: 04.2f} nm'.format(xb_e * 1e9),
                     xy=(0.52, -2.2), xytext=(0.52, -2.2),
                     fontsize=20, family='serif')
        ax1.annotate(r'$x_{b,h}=$' + '{: 04.2f} nm'.format(xb_h * 1e9),
                     xy=(0.52, -2.6), xytext=(0.52, -2.6),
                     fontsize=20, family='serif')
        ax1.plot(xi_m, eta_cm * vth, 'k--',
                 xi_m, ubi_m * vth, 'm--',
                 xi, eta_c * vth, 'b--',
                 xi, eta_v * vth, 'g--',
                 xi_s, eta_cs * vth, 'b--',
                 xi_s, eta_vs * vth, 'g--',
                 xi_s, eta_fss * vth, 'k',
                 lw=4)

        y_min = -3.0
        y_max = 3.5
        height = -y_min + y_max
        ax1.set_xlim([-0.2, 1.2])
        ax1.set_ylim([y_min, y_max])
        plt.xlabel(r'$\xi=x/x_b$', fontsize=20, color='black')
        plt.ylabel('Energy (eV)', fontsize=20, color='black')
        ax1.add_patch(
            patches.Rectangle(
                (0.0, y_min),  # (x,y)
                1.0,  # width
                height,  # height
                facecolor="black",
                alpha=0.1,
                linewidth=0,
            )
        )
        fig.savefig(STATIC_DIR + '/' + fct_name + '/fig_' + str(i) + '.png',
                    dpi=dpi, bbox_inches='tight')
        plt.cla()
    plt.close(fig)
    gen_mp4(fct_name)


def anim_transmission_coefficient():
    r"""

    """
    fct_name = inspect.stack()[0][3]
    if not os.path.isdir(STATIC_DIR.child(fct_name)):
        os.mkdir(STATIC_DIR.child(fct_name))
    if os.path.exists(STATIC_DIR.child(fct_name + '.mp4')):
        os.remove(STATIC_DIR.child(fct_name + '.mp4'))
    num_v = 101
    num_e = 1001
    te = np.zeros([num_v, num_e])
    th = np.zeros([num_v, num_e])

    nd = 1.0e25
    bar = Barrier(Nd=nd)
    eta_init = np.maximum(bar.values['eta_ceq'], bar.values['eta_fI'])
    w = Wave(eta_init, bar,
             mI=M_E, mb=M_E, mII=M_E,
             mhI=0.25 * M_E, mhb=0.25 * M_E, mhII=0.25 * M_E)
    vbi = bar.values['Vbi']
    eta = np.linspace(-100.0, 50.0, num_e)
    v_a = np.linspace(-0.5 * vbi, 1.5 * vbi, num_v)

    dpi = 96
    fig = plt.figure(figsize=(909 / dpi, 646 / dpi), dpi=dpi)
    ax1 = fig.add_subplot(111)
    for i in progressbar.progressbar(range(0, num_v)):
        time.sleep(0.02)
        val_va = v_a[i]
        bar.set_applied_voltage(val_va)
        for j in range(0, num_e):
            val_eta = eta[j]
            te[i][j] = get_transmission_coefficient(w, val_eta, bar)[0]
            th[i][j] = get_transmission_coefficient(w, val_eta, bar)[1]
        ax1.annotate(
            r'$V_a$' + ' = {0:=1.2f}'.format(val_va / vbi) + r'$V_{bi}$',
            xy=(-40, 0.8), xytext=(-40, 0.8),
            fontsize=20, family='serif')
        ax1.set_xlim([-100.0, 50.0])
        ax1.set_ylim([0.0, 1.1])
        ax1.plot(eta, te[i], '-k', eta, th[i], '-g',
                 bar.values['eta_ceq'] * np.ones(num_e),
                 np.linspace(0, 1.1, num_e), '--k',
                 bar.values['eta_veq'] * np.ones(num_e),
                 np.linspace(0, 1.1, num_e), '--g',
                 linewidth=2.0,
                 # Vbi*np.ones(num), np.linspace(-100.0,100.0,num),'k--',
                )
        plt.xlabel(r'$\eta$', fontsize=20, color='black')
        plt.ylabel('Transmission Coefficient', fontsize=20, color='black')
        fig.savefig(STATIC_DIR + '/' + fct_name + '/fig_' + str(i) + '.png',
                    dpi=dpi, bbox_inches='tight')
        plt.cla()
    plt.close(fig)
    gen_mp4(fct_name)


def wave_function():
    r"""

    :return:
    """
    fct_name = inspect.stack()[0][3]
    if not os.path.isdir(STATIC_DIR.child(fct_name)):
        os.mkdir(STATIC_DIR.child(fct_name))
    num = 101
    b = Barrier(Nd=5e24)
    eta_min_e = np.maximum(b.values['eta_ceq'], b.values['eta_fI'])
    w = Wave(eta_min_e, b)
    eta_max_e = eta_min_e + 1.5 * b.values['eta_be']
    x, y = np.mgrid[0.0:num:1.0, 0.0:num:1.0] / (num - 1)
    eta_m = eta_min_e + y * (eta_max_e - eta_min_e)
    eta_v = eta_m[0]
    xi = x.transpose()[0]
    sol_r = np.zeros((num, num))
    sol_i = np.zeros((num, num))
    i = 0
    for eta in eta_v:
        w.set_eta(eta)
        psi = w.compute_psi_figure('exact', xi)
        sol_r[i] = psi[0]
        sol_i[i] = psi[1]
        i += 1
    sol_n = np.absolute(sol_r + 1j * sol_i)
    display = sol_n
    dpi = 96
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(display)
    fig = plt.figure(figsize=(937 / dpi, 643 / dpi), dpi=dpi)
    fig.add_subplot(111)
    plt.colorbar(m)
    plt.contourf(x, eta_m * cst.k * 300.0 / cst.e, display, 200)
    plt.xlabel(r'$\xi=x/x_b$', fontsize=20, color='black')
    plt.ylabel('Energy (eV)', fontsize=20, color='black')
    fig.savefig(STATIC_DIR + '/' + fct_name + '/fig_' + fct_name + '.png',
                dpi=dpi, bbox_inches='tight')
    plt.close(fig)


def current_density():
    r"""

    """
    fct_name = inspect.stack()[0][3]
    if not os.path.isdir(STATIC_DIR.child(fct_name)):
        os.mkdir(STATIC_DIR.child(fct_name))
    EPS = np.finfo(float).eps
    me = cst.m_e
    q = cst.e
    kb = cst.k
    T = 300.0
    hbar = cst.hbar
    A = me * q * (kb) ** 2 / (2 * np.pi ** 2.0 * hbar ** 3.0)
    numV = 21
    numE = 1001
    numD = 6
    inte = np.zeros([numV, numE])
    inth = np.zeros([numV, numE])
    Jn = np.zeros([numD, numV])
    Jp = np.zeros([numD, numV])
    Jth = np.zeros(numV)

    eta = np.linspace(-100.0, 50.0, numE)
    Nds = 5.0 * np.logspace(23, 25, numD)

    if not os.path.exists(STATIC_DIR + '/' + fct_name + '/test.txt'):
        for k in range(0, numD):
            b = Barrier(Nd=Nds[k], Na=0)
            Vbi = b.values['Vbi']
            if b.values['xb_e'] > 0.0:
                # electron barrier
                print('Electron barrier: Nd = {0:=5E}, Va = 0.0, '
                        'Vbi = {1:=5E}'.format(Nds[k], Vbi))
                eta_init = np.maximum(b.values['eta_ceq'],
                                      b.values['eta_fI'])
            else:
                # hole barrier
                print('Hole barrier: Nd = {0:=5E}, Va = 0.0, '
                      'Vbi = {1:=5E}'.format(Nds[k], Vbi))
                eta_init = np.minimum(b.values['eta_veq'],
                                      b.values['eta_fI'])
            w = Wave(eta_init, b,
                     mI=M_E, mb=M_E, mII=M_E,
                     mhI=0.25 * M_E, mhb=0.25 * M_E, mhII=0.25 * M_E)
            Va = np.linspace(-0.3 * Vbi, 0.1 * Vbi, numV)
            for i in range(0, numV):
                val_Va = Va[i]
                b.set_applied_voltage(val_Va)
                eta_min = np.maximum(b.values['eta_ceq'],
                                     b.values['eta_fI'])
                eta_max = np.minimum(b.values['eta_veq'],
                                     b.values['eta_fI'])
                for j in range(0, numE):
                    val_eta = eta[j]
                    if val_eta >= b.values['eta_ceq']:
                        inte[i][j] = (compute_se(val_eta, b) *
                                      get_transmission_coefficient(
                                          w, val_eta, b)[0])
                        if abs(inte[i][j]) < EPS:
                            inte[i][j] = EPS
                    else:
                        inth[i][j] = (compute_sh(val_eta, b) *
                                      get_transmission_coefficient(
                                          w, val_eta, b)[1])
                        if abs(inth[i][j]) < EPS:
                            inth[i][j] = EPS
                # Perform the integration
                sol_inte = integrate.simps(inte[i], eta, 0.0)
                sol_inth = integrate.simps(inth[i], eta, 0.0)
                Jn[k][i] = -A * T ** 2.0 * sol_inte
                Jp[k][i] = A * T ** 2.0 * sol_inth
                Jth[i] = -A * T ** 2.0 * np.exp(-b.values['eta_be']) * \
                         (1.0 - np.exp(b.values['ua']))
                print('Va = {0:=5E}, Jn = {1:=5E}, Jp = {2:=5E}'.
                      format(val_Va, Jn[k][i], Jp[k][i]))
            out = np.zeros([numV, numD * 2 + 2])
            out[:, 0] = Va
            out[:, 1] = Jth
            for i in range(0, numD):
                out[:, 2 * i + 2] = Jn[i]
                out[:, 2 * i + 3] = Jp[i]
            np.savetxt(STATIC_DIR + '/' + fct_name + '/test.txt', out, fmt='%1.4e')
    else:
        values = np.loadtxt(STATIC_DIR + '/' + fct_name + '/test.txt')
        Va = values[:, 0]
        Jth = values[:, 1]
        for i in range(0, numD):
            Jn[i] = values[:, 2 * i + 2]
            Jp[i] = values[:, 2 * i + 3]

    dpi = 96
    fig = plt.figure(figsize=(913 / dpi, 646 / dpi), dpi=dpi)
    ax1 = fig.add_subplot(111)
    index = 2
    ymin = np.min([np.min(Jn[index]), np.min(Jp[index])])
    ymax = np.max([np.max(Jn[index]), np.max(Jp[index])]) + EPS
    if ymin < 0.0:
        ymin = 1.1 * ymin
    else:
        ymin = 0.9 * ymin
    if ymax < 0.0:
        ymax = 0.9 * ymax
    else:
        ymax = 1.1 * ymax
    ax1.set_xlim([np.min(Va), np.max(Va)])
    ax1.set_ylim([ymin, ymax])
    ax1.plot(Va, Jth, '--k', label='Thermionic')
    ax1.plot(Va, Jn[0] + Jp[0], '-b',
             label='Nd = {0:=2.2E} '.format(
             Nds[0] / 1.0E06) + r'cm$^{-3}$')
    ax1.plot(Va, Jn[1] + Jp[1], '-g',
             label='Nd = {0:=2.2E} '.format(
             Nds[1] / 1.0E06) + r'cm$^{-3}$')
    ax1.plot(Va, Jn[2] + Jp[2], '-m',
             label='Nd = {0:=2.2E} '.format(
             Nds[2] / 1.0E06) + r'cm$^{-3}$')
    ax1.plot(Va, Jn[3] + Jp[3], '-r',
             label='Nd = {0:=2.2E} '.format(
             Nds[3] / 1.0E06) + r'cm$^{-3}$')
    plt.xlabel(r'$V_a$ (V)', fontsize=20, color='black')
    plt.ylabel(r'J (Am$^{-2})$', fontsize=20, color='black')
    ax1.legend(loc='upper left', shadow=True)
    fig.savefig(STATIC_DIR + '/' + fct_name + '/fig_' + fct_name + '.png',
                dpi=dpi, bbox_inches='tight')
    plt.close(fig)
