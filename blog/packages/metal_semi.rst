metal_semi
----------

contact.barrier
^^^^^^^^^^^^^^^

.. automodule:: metal_semi.contact.barrier
    :members:
    :undoc-members:
    :show-inheritance:

contact.pcfs
^^^^^^^^^^^^

.. automodule:: metal_semi.contact.pcfs
    :members:
    :undoc-members:
    :show-inheritance:
