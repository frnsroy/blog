# -*- coding: utf-8 -*-
"""
doc.conf.py
January 12, 2019
@author Francois Roy
"""
import os
import sys
import alabaster
import ablog
from unipath import Path


# ablog build -w '../_build' # in the directory containing this file
ablog_website = '../_build'  # doesn't work
ablog_builder = 'html'

# Get the project root dir, which is the parent dir of this
ROOT_DIR = Path(os.path.abspath(__file__)).ancestor(2)

# Insert the project root dir as the first element in the PYTHONPATH.
sys.path.insert(0, ROOT_DIR)

plot_include_source = False
plot_html_show_formats = False
plot_html_show_source_link = False

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.extlinks',
    'sphinx.ext.mathjax',
    'sphinx.ext.viewcode',
    'sphinx.ext.graphviz',
    'alabaster',
    'ablog',
    'sphinxcontrib.bibtex',
    'matplotlib.sphinxext.plot_directive',
    'recommonmark',
    'youtube',
    'contentui',
]

project = u'Blog'
copyright = u'2019, François Roy'
master_doc = 'index'
source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}
exclude_patterns = ['_build']

templates_path = [ablog.get_html_templates_path()]
templates_path.append('_templates')
# add in each page
rst_prolog = """
.. role:: red
"""

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
}
extlinks = {
    'wiki': ('https://en.wikipedia.org/wiki/%s', ''),
    'repo': ('https://bitbucket.org/frnsroy/%s', ''),
}
pygments_style = 'sphinx'

html_title = "François Roy"
html_static_path = ['_static']
html_use_index = True
html_domain_indices = False
html_show_sourcelink = False  # True
html_favicon = '_static/icon.png'

html_style = 'alabaster.css'
html_theme = 'alabaster'
html_sidebars = {
    '**': ['about.html', 'postcard.html',
           # 'navigation.html',
           'recentposts.html',
           'tagcloud.html', 'categories.html',
           'archives.html', 'searchbox.html']
}
html_theme_path = [alabaster.get_path()]
html_theme_options = {
    'description': 'A blog about physics and arts.',
    'logo': 'logo.png',
    'show_powered_by': False,
}

blog_title = 'Blog'
blog_baseurl = 'https://blog.froy.ca'
blog_authors = {
    'François Roy': ('François Roy', 'https://www.froy.ca'),
}
blog_locations = {
    'Roxbury': ('Roxbury, MA', 'https://en.wikipedia.org/wiki/Roxbury,_Boston'),
}
blog_languages = {
    'en': ('English', None),
}
blog_default_language = 'en'
blog_default_location = 'Roxbury'
disqus_shortname = 'frnsroy'
disqus_pages = False  # for non-post pages
